/* bookup-tags.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-tags.h"
#include "bookup-marshal.h"
#include "bookup-page.h"

enum
{
  ACTIVATE,
  N_SIGNALS
};

struct _BookupTags
{
  GtkBox parent_instance;
  GListModel *model;
  GtkSingleSelection *selection;
  GMenuModel *menu;
};

G_DEFINE_FINAL_TYPE (BookupTags, bookup_tags, GTK_TYPE_BOX)

static unsigned int signals[N_SIGNALS];

static void
activate_cb (BookupTags *self, unsigned int position, GtkListView *listview)
{
  g_autoptr (BookupPage) page = NULL;

  g_return_if_fail (BOOKUP_IS_TAGS (self));
  g_return_if_fail (GTK_IS_LIST_VIEW (listview));

  page = g_list_model_get_item (self->model, position);

  if (BOOKUP_IS_PAGE (page))
    {
      g_signal_emit (self, signals[ACTIVATE], 0, page);

      gtk_selection_model_select_item (GTK_SELECTION_MODEL (self->selection),
                                       position,
                                       TRUE);
    }
}

static void
select_tag (BookupTags *self, unsigned int tag_id)
{
  BookupPage *tag;
  unsigned int i, n_items;
  n_items = g_list_model_get_n_items (self->model);

  for (i = 0; i < n_items; i++)
    {
      tag = BOOKUP_PAGE (g_list_model_get_item (self->model, i));

      if (bookup_page_get_id (tag) == tag_id)
        {
          gtk_selection_model_select_item (GTK_SELECTION_MODEL (self->selection), i, TRUE);
          break;
        }
    }
}

int
bookup_tags_get_selected_tag_id (BookupTags *self)
{
  GObject *item = gtk_single_selection_get_selected_item (self->selection);

  return BOOKUP_IS_PAGE (item) ? (int) bookup_page_get_id (BOOKUP_PAGE (item)) : -1;
}

static void
on_tag_unmap_cb (GtkWidget *widget, gpointer user_data)
{
  GtkWidget *child;

  g_return_if_fail (GTK_IS_WIDGET (widget));
  g_return_if_fail (GTK_IS_LIST_ITEM (user_data));

  child = gtk_widget_get_first_child (widget);

  if (GTK_IS_POPOVER_MENU (child))
    /* Remove the popover menu created in tag_button_pressed_cb */
    gtk_widget_unparent (child);
}

static void
tag_button_pressed_cb (GtkGestureClick *click,
                       int n_press,
                       G_GNUC_UNUSED gdouble x,
                       G_GNUC_UNUSED gdouble y,
                       gpointer user_data)
{
  GtkWidget *widget;
  GObject *item;
  BookupTags *self;
  GtkPopover *popover;

  g_return_if_fail (GTK_IS_LIST_ITEM (user_data));

  if (n_press > 1)
    return;

  item = gtk_list_item_get_item (GTK_LIST_ITEM (user_data));
  widget = gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (click));
  self = BOOKUP_TAGS (gtk_widget_get_ancestor (widget, BOOKUP_TYPE_TAGS));
  select_tag (self, bookup_page_get_id (BOOKUP_PAGE (item)));

  popover = g_object_new (GTK_TYPE_POPOVER_MENU,
                          "menu-model", self->menu,
                          "has-arrow", TRUE,
                          "position", GTK_POS_RIGHT,
                          NULL);
  gtk_widget_set_parent (GTK_WIDGET (popover), widget);
  gtk_popover_popup (popover);
}

void
bookup_tags_set_model (BookupTags *self, GListModel *model)
{
  if (self->model)
    g_object_unref (self->model);

  self->model = model;
  gtk_single_selection_set_model (self->selection, self->model);
}

void
bookup_tags_remove_all (BookupTags *self)
{
  if (self->model)
    {
      g_object_unref (self->model);
      self->model = NULL;
      gtk_single_selection_set_model (self->selection, self->model);
    }
}

static void
bookup_tags_class_init (BookupTagsClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/gitlab/ilhooq/Bookup/bookup-tags.ui");
  gtk_widget_class_bind_template_child (widget_class, BookupTags, selection);
  gtk_widget_class_bind_template_child (widget_class, BookupTags, menu);
  gtk_widget_class_bind_template_callback (widget_class, activate_cb);
  gtk_widget_class_bind_template_callback (widget_class, tag_button_pressed_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_tag_unmap_cb);

  signals[ACTIVATE] = g_signal_new ("activate",
                                    G_TYPE_FROM_CLASS (klass),
                                    G_SIGNAL_RUN_LAST,
                                    0,
                                    NULL, NULL,
                                    bookup_marshal_VOID__OBJECT,
                                    G_TYPE_NONE,
                                    1, BOOKUP_TYPE_PAGE);

  g_signal_set_va_marshaller (signals[ACTIVATE],
                              G_TYPE_FROM_CLASS (klass),
                              bookup_marshal_VOID__OBJECTv);
}

static void
bookup_tags_init (BookupTags *self)
{
  self->model = NULL;
  gtk_widget_init_template (GTK_WIDGET (self));
}
