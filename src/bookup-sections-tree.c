/* bookup-sections-tree.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-sections-tree.h"

typedef struct _SearchNode SearchNode;

struct _SearchNode
{
  unsigned int id;
  GNode *result;
};

struct _BookupSectionsTree
{
  GObject parent_instance;

  GNode *node;
};

static GNode *sections_tree = NULL;

static GNode *build_tree (GListModel *list);
static void tree_free (GNode *node);
static GNode *tree_find (GNode *node, unsigned int id);
static gboolean tree_is_ancestor (GNode *node, unsigned int ancestor_id);
static void tree_remove (GNode *node, unsigned int id);
static void tree_add (GNode *tree, BookupSection *section);

static unsigned int
list_model_get_n_items (GListModel *model)
{
  BookupSectionsTree *self = BOOKUP_SECTIONS_TREE (model);

  return g_node_n_children (self->node);
}

static GType
list_model_get_item_type (G_GNUC_UNUSED GListModel *model)
{
  return BOOKUP_TYPE_SECTION;
}

static gpointer
list_model_get_item (GListModel *model, guint position)
{
  BookupSectionsTree *self = BOOKUP_SECTIONS_TREE (model);

  unsigned int n_children = g_node_n_children (self->node);
  GNode *node;

  if (position >= n_children)
    return NULL;

  if (position == 0)
    node = g_node_first_child (self->node);
  else if (position == n_children - 1)
    node = g_node_last_child (self->node);
  else
    node = g_node_nth_child (self->node, position);

  return g_object_ref (node->data);
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_n_items = list_model_get_n_items;
  iface->get_item_type = list_model_get_item_type;
  iface->get_item = list_model_get_item;
}

G_DEFINE_TYPE_WITH_CODE (BookupSectionsTree,
                         bookup_sections_tree,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

static void
bookup_sections_tree_class_init (G_GNUC_UNUSED BookupSectionsTreeClass *klass)
{
}

static void
bookup_sections_tree_init (G_GNUC_UNUSED BookupSectionsTree *self)
{
}

BookupSectionsTree *
bookup_sections_tree_new (BookupSection *section, GListModel *list)
{
  BookupSectionsTree *self = g_object_new (BOOKUP_TYPE_SECTIONS_TREE, NULL);

  if (G_IS_LIST_MODEL (list))
    {
      tree_free (sections_tree);
      sections_tree = build_tree (list);
    }

  self->node = (section == NULL) ? sections_tree : tree_find (sections_tree, bookup_section_get_id (section));

  return self;
}

gboolean
bookup_sections_tree_section_has_children (BookupSectionsTree *self, BookupSection *section)
{
  GNode *node = tree_find (self->node, bookup_section_get_id (section));
  return (node && node->children) ? TRUE : FALSE;
}

gboolean
bookup_sections_tree_section_is_ancestor (BookupSectionsTree *self,
                                          BookupSection *ancestor,
                                          BookupSection *section)
{
  GNode *node = tree_find (self->node, bookup_section_get_id (section));

  if (node)
    return tree_is_ancestor (node, bookup_section_get_id (ancestor));

  return FALSE;
}

void
bookup_sections_tree_remove (BookupSectionsTree *self, BookupSection *section)
{
  unsigned int n_children = g_node_n_children (self->node);
  unsigned int pos;
  GNode *node;

  for (pos = 0; pos < n_children; pos++)
    {
      node = g_node_nth_child (self->node, pos);
      if (bookup_section_get_id (section) == bookup_section_get_id (BOOKUP_SECTION (node->data)))
        break;
    }

  tree_remove (self->node, bookup_section_get_id (section));
  g_list_model_items_changed (G_LIST_MODEL (self), pos, 1, 0);
}

void
bookup_sections_tree_add_section (BookupSectionsTree *self, BookupSection *section)
{
  g_return_if_fail (BOOKUP_IS_SECTIONS_TREE (self));

  unsigned int pos, deleted;
  unsigned int n_children = g_node_n_children (self->node);
  GNode *child;

  deleted = 0;

  if (bookup_section_get_parent_id (section))
    {

      for (pos = 0; pos < n_children; pos++)
        {
          child = g_node_nth_child (self->node, pos);

          if (bookup_section_get_id (BOOKUP_SECTION (child->data)) ==
              bookup_section_get_parent_id (section))
            {
              deleted = 1;
              break;
            }
        }
    }
  else
    {
      pos = n_children;
    }

  tree_add (self->node, section);
  g_list_model_items_changed (G_LIST_MODEL (self), pos, deleted, 1);
}

void
bookup_sections_tree_remove_all (BookupSectionsTree *self)
{
  g_return_if_fail (self->node == sections_tree);

  unsigned int n_items = g_node_n_children (self->node);

  tree_free (self->node);

  g_list_model_items_changed (G_LIST_MODEL (self), 0, n_items, 0);
}

static gboolean
find_section_node_cb (GNode *node, gpointer data)
{
  SearchNode *search_node = (SearchNode *) data;

  if (node->data && bookup_section_get_id (node->data) == search_node->id)
    {
      search_node->result = node;
      return TRUE;
    }

  return FALSE;
}

static GNode *
tree_find (GNode *node, unsigned int id)
{
  SearchNode search;
  search.id = id;
  search.result = NULL;

  g_node_traverse (node,
                   G_LEVEL_ORDER,
                   G_TRAVERSE_ALL,
                   -1,
                   find_section_node_cb,
                   (gpointer) &search);

  return search.result;
}

static void
tree_remove (GNode *node, unsigned int id)
{
  GNode *find_node = tree_find (node, id);
  g_node_unlink (find_node);
  g_node_destroy (find_node);
}

static void
tree_add (GNode *tree, BookupSection *section)
{
  unsigned int parent_id = bookup_section_get_parent_id (section);

  if (parent_id)
    {
      GNode *parent = tree_find (tree, parent_id);

      if (parent)
        g_node_append (parent, g_node_new (section));

      return;
    }

  g_node_append (tree, g_node_new (section));
}

static gboolean
tree_is_ancestor (GNode *node, unsigned int ancestor_id)
{
  GNode *parent = node->parent;
  BookupSection *section;

  while (parent)
    {
      GNode *current = parent;

      if (current->data)
        {
          section = BOOKUP_SECTION (current->data);

          if (bookup_section_get_id (section) == ancestor_id)
            {
              return TRUE;
            }
        }

      parent = current->parent;
    }

  return FALSE;
}

static GNode *
build_tree (GListModel *list)
{
  unsigned int i, n_items;
  GNode *root;
  GNode *child;
  BookupSection *section;

  g_assert (g_list_model_get_item_type (list) == BOOKUP_TYPE_SECTION);

  root = g_node_new (NULL);
  n_items = g_list_model_get_n_items (list);

  for (i = 0; i < n_items; i++)
    g_node_append (root, g_node_new (g_list_model_get_item (list, i)));

  child = root->children;

  /* Build the sections tree */
  while (child)
    {
      section = BOOKUP_SECTION (child->data);
      GNode *current = child;
      child = child->next;

      if (bookup_section_get_parent_id (section))
        {
          GNode *parent = tree_find (root, bookup_section_get_parent_id (section));

          if (parent)
            {
              g_node_unlink (current);
              g_node_append (parent, current);
            }
        }
    }

  g_object_unref (list);

  return root;
}

static void
tree_free (GNode *node)
{
  if (node)
    {
      while (node->children)
        {
          g_node_unlink (node->children);

          if (node->children)
            g_node_destroy (node->children);
        }

      node = NULL;
    }
}
