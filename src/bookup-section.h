/* bookup-database.h
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BOOKUP_TYPE_SECTION (bookup_section_get_type ())

G_DECLARE_FINAL_TYPE (BookupSection, bookup_section, BOOKUP, SECTION, GObject)

BookupSection *bookup_section_new ();

void bookup_section_set_id (BookupSection *, unsigned int);
void bookup_section_set_name (BookupSection *, const char *);
void bookup_section_set_rgb (BookupSection *, const char *);
void bookup_section_set_parent_id (BookupSection *, unsigned int);

unsigned int bookup_section_get_id (BookupSection *);
char *bookup_section_get_name (BookupSection *);
char *bookup_section_get_rgb (BookupSection *);
unsigned int bookup_section_get_parent_id (BookupSection *);

G_END_DECLS
