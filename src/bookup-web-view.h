/* bookup-web-view.h
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include <webkit/webkit.h>

G_BEGIN_DECLS

#define BOOKUP_TYPE_WEB_VIEW (bookup_web_view_get_type ())

G_DECLARE_FINAL_TYPE (BookupWebView, bookup_web_view, BOOKUP, WEB_VIEW, WebKitWebView)

void bookup_web_view_load_html (BookupWebView *self, const char *html);

void bookup_web_view_set_custom_css (BookupWebView *self, const char *css);
void bookup_web_view_set_custom_script (BookupWebView *self, const char *script);

gboolean bookup_web_view_print (BookupWebView *self, const GFile *file);

G_END_DECLS
