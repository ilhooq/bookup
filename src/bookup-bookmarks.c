/* bookup-bookmarks.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-bookmarks.h"
#include "bookup-marshal.h"
#include "bookup-page.h"

enum
{
  ACTIVATE,
  N_SIGNALS
};

struct _BookupBookmarks
{
  GtkBox parent_instance;
  GListModel *model;
  GtkSingleSelection *selection;
};

G_DEFINE_FINAL_TYPE (BookupBookmarks, bookup_bookmarks, GTK_TYPE_BOX)

static unsigned int signals[N_SIGNALS];

static void
activate_cb (BookupBookmarks *self, unsigned int position, GtkListView *listview)
{
  g_autoptr (BookupPage) page = NULL;

  g_return_if_fail (BOOKUP_IS_BOOKMARKS (self));
  g_return_if_fail (GTK_IS_LIST_VIEW (listview));

  page = g_list_model_get_item (self->model, position);
  g_assert (BOOKUP_IS_PAGE (page));

  g_signal_emit (self, signals[ACTIVATE], 0, page);

  gtk_selection_model_select_item (
      GTK_SELECTION_MODEL (self->selection),
      position,
      TRUE);
}

void
bookup_bookmarks_set_model (BookupBookmarks *self, GListModel *model)
{
  if (self->model)
    g_object_unref (self->model);

  self->model = model;
  gtk_single_selection_set_model (self->selection, self->model);
}

void
bookup_bookmarks_remove_all (BookupBookmarks *self)
{
  if (self->model)
    {
      g_object_unref (self->model);
      self->model = NULL;
      gtk_single_selection_set_model (self->selection, self->model);
    }
}

static void
bookup_bookmarks_class_init (BookupBookmarksClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  gtk_widget_class_set_template_from_resource (
      widget_class,
      "/org/gnome/gitlab/ilhooq/Bookup/bookup-bookmarks.ui");

  gtk_widget_class_bind_template_child (widget_class, BookupBookmarks, selection);
  gtk_widget_class_bind_template_callback (widget_class, activate_cb);

  signals[ACTIVATE] = g_signal_new ("activate",
                                    G_TYPE_FROM_CLASS (klass),
                                    G_SIGNAL_RUN_LAST,
                                    0,
                                    NULL, NULL,
                                    bookup_marshal_VOID__OBJECT,
                                    G_TYPE_NONE,
                                    1, BOOKUP_TYPE_PAGE);

  g_signal_set_va_marshaller (signals[ACTIVATE],
                              G_TYPE_FROM_CLASS (klass),
                              bookup_marshal_VOID__OBJECTv);
}

static void
bookup_bookmarks_init (BookupBookmarks *self)
{
  self->model = NULL;
  gtk_widget_init_template (GTK_WIDGET (self));
}
