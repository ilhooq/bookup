/* bookup-sections-tree.h
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "bookup-section.h"
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BOOKUP_TYPE_SECTIONS_TREE (bookup_sections_tree_get_type ())

G_DECLARE_FINAL_TYPE (BookupSectionsTree, bookup_sections_tree, BOOKUP, SECTIONS_TREE, GObject)

BookupSectionsTree *
bookup_sections_tree_new (BookupSection *section, GListModel *list);

gboolean
bookup_sections_tree_section_has_children (BookupSectionsTree *self, BookupSection *section);

gboolean
bookup_sections_tree_section_is_ancestor (BookupSectionsTree *self,
                                          BookupSection *ancestor,
                                          BookupSection *section);

void
bookup_sections_tree_remove (BookupSectionsTree *self, BookupSection *section);

void
bookup_sections_tree_add_section (BookupSectionsTree *self, BookupSection *section);

void
bookup_sections_tree_remove_all (BookupSectionsTree *self);

G_END_DECLS
