/* bookup-sections-view.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-sections-view.h"
#include "bookup-marshal.h"
#include "bookup-page.h"
#include "bookup-section-icon.h"
#include "bookup-section.h"
#include "bookup-sections-tree.h"

enum
{
  ACTIVATE,
  DROP_PAGE,
  DROP_SECTION,
  N_SIGNALS
};

struct _BookupSectionsView
{
  GtkWidget parent_instance;
  GMenuModel *menu;
  GtkLabel *root_section;
  GtkSingleSelection *selection;
  GtkTreeListModel *tree_model;
};

G_DEFINE_FINAL_TYPE (BookupSectionsView, bookup_sections_view, GTK_TYPE_WIDGET)

static unsigned int signals[N_SIGNALS];

static void
select_section_row (BookupSectionsView *self, GtkTreeListRow *row)
{
  unsigned int pos = gtk_tree_list_row_get_position (row);
  gtk_selection_model_select_item (
      GTK_SELECTION_MODEL (self->selection),
      pos,
      TRUE);
}

gboolean
bookup_sections_view_on_drop (GtkDropTarget *drop_target,
                              const GValue *value,
                              G_GNUC_UNUSED double x,
                              G_GNUC_UNUSED double y,
                              BookupSectionsView *self)
{
  BookupSection *section;
  GtkTreeListRow *row = NULL;
  GtkWidget *widget;
  GListModel *model;
  gpointer data;

  g_assert (GTK_IS_DROP_TARGET (drop_target));
  g_assert (BOOKUP_IS_SECTIONS_VIEW (self));

  widget = gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (drop_target));

  if (GTK_IS_TREE_EXPANDER (widget))
    {
      row = gtk_tree_expander_get_list_row (GTK_TREE_EXPANDER (widget));
      section = gtk_tree_list_row_get_item (row);
    }
  else
    {
      /* The widget corresponds to the root section */
      section = bookup_section_new ();
      bookup_section_set_id (section, 0);
    }

  data = g_value_get_object (value);

  if (BOOKUP_IS_PAGE (data) &&
      bookup_page_get_section_id (BOOKUP_PAGE (data)) != bookup_section_get_id (section))
    {
      g_signal_emit (self, signals[DROP_PAGE], 0,
                     bookup_page_get_id (BOOKUP_PAGE (data)),
                     bookup_section_get_id (section));
      return TRUE;
    }
  else if (GTK_IS_TREE_LIST_ROW (data))
    {
      BookupSection *drag_section = gtk_tree_list_row_get_item (GTK_TREE_LIST_ROW (data));
      g_return_val_if_fail (BOOKUP_IS_SECTION (drag_section), FALSE);

      /* clang-format off */
      if (bookup_section_get_parent_id (drag_section) != bookup_section_get_id (section)
          && bookup_section_get_id (drag_section) != bookup_section_get_id (section))
        /* clang-format on */
        {
          GtkTreeListRow *parent = NULL;
          bookup_section_set_parent_id (drag_section, bookup_section_get_id (section));

          /* Get the tree row model if exists otherwise the root model */
          parent = gtk_tree_list_row_get_parent (GTK_TREE_LIST_ROW (data));
          model = (parent) ? gtk_tree_list_row_get_children (parent) : gtk_tree_list_model_get_model (self->tree_model);

          bookup_sections_tree_remove (BOOKUP_SECTIONS_TREE (model), drag_section);

          parent = GTK_IS_TREE_LIST_ROW (row) ? gtk_tree_list_row_get_parent (row) : NULL;
          /* Get the tree row model if exists otherwise the root model */
          model = (parent) ? gtk_tree_list_row_get_children (parent) : gtk_tree_list_model_get_model (self->tree_model);

          bookup_sections_tree_add_section (BOOKUP_SECTIONS_TREE (model), drag_section);

          g_signal_emit (self, signals[DROP_SECTION], 0,
                         bookup_section_get_id (drag_section),
                         bookup_section_get_id (section));

          return TRUE;
        }
    }

  return FALSE;
}

static GdkContentProvider *
on_drag_prepare (GtkDragSource *source,
                 G_GNUC_UNUSED double x,
                 G_GNUC_UNUSED double y,
                 G_GNUC_UNUSED gpointer user_data)
{
  GtkTreeExpander *expander;
  GtkTreeListRow *row;

  g_assert (GTK_IS_DRAG_SOURCE (source));

  expander = GTK_TREE_EXPANDER (
      gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (source)));
  row = gtk_tree_expander_get_list_row (expander);

  return gdk_content_provider_new_typed (GTK_TYPE_TREE_LIST_ROW, row);
}

static void
on_drag_begin (GtkDragSource *source,
               GdkDrag *drag,
               G_GNUC_UNUSED gpointer user_data)
{
  g_autoptr (GdkPaintable) paintable = NULL;
  GtkWidget *widget;

  g_assert (GDK_IS_DRAG (drag));
  g_assert (GTK_IS_DRAG_SOURCE (source));

  /* Get our GtkTreeExpander */
  widget = gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (source));

  /* But snapshot the parent to get row content */
  widget = gtk_widget_get_parent (widget);

  if ((paintable = gtk_widget_paintable_new (widget)))
    {
      GtkSnapshot *snapshot = gtk_snapshot_new ();
      double width = gdk_paintable_get_intrinsic_width (paintable);
      double height = gdk_paintable_get_intrinsic_height (paintable);
      g_autoptr (GdkPaintable) with_parent = NULL;

      gdk_paintable_snapshot (paintable, snapshot, width, height);

      with_parent = gtk_snapshot_free_to_paintable (snapshot, &GRAPHENE_SIZE_INIT (width, height));
      gtk_drag_source_set_icon (source, paintable, 0, 0);
    }
}

static void
sections_view_onButtonPressed (GtkGestureClick *click,
                               int n_press,
                               G_GNUC_UNUSED gdouble x,
                               G_GNUC_UNUSED gdouble y,
                               gpointer user_data)
{
  GtkTreeExpander *expander;
  GtkListItem *item;
  GtkTreeListRow *row;
  GtkWidget *widget;
  BookupSectionsView *view;
  GtkPopover *popover;

  g_assert (GTK_IS_LIST_ITEM (user_data));

  if (n_press > 1)
    return;

  expander = GTK_TREE_EXPANDER (
      gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (click)));
  item = GTK_LIST_ITEM (user_data);
  row = gtk_list_item_get_item (item);
  widget = gtk_list_item_get_child (item);

  view = BOOKUP_SECTIONS_VIEW (gtk_widget_get_ancestor (widget, BOOKUP_TYPE_SECTIONS_VIEW));

  g_return_if_fail (BOOKUP_IS_SECTIONS_VIEW (view));

  select_section_row (view, row);

  popover = g_object_new (GTK_TYPE_POPOVER_MENU,
                          "menu-model", view->menu,
                          "has-arrow", TRUE,
                          "position", GTK_POS_RIGHT,
                          NULL);

  gtk_widget_set_parent (GTK_WIDGET (popover), GTK_WIDGET (expander));
  gtk_popover_popup (popover);
}

static void
on_expander_destroy (GtkTreeExpander *expander, G_GNUC_UNUSED gpointer user_data)
{
  GtkWidget *widget;

  g_return_if_fail (GTK_IS_TREE_EXPANDER (expander));

  widget = gtk_widget_get_last_child (GTK_WIDGET (expander));

  if (GTK_IS_POPOVER_MENU (widget))
    {
      /* Remove the popover menu created in sections_view_onButtonPressed */
      gtk_widget_unparent (widget);
    }
}

static void
setup_list_cb (BookupSectionsView *self,
               GtkListItem *item,
               GtkSignalListItemFactory *factory)
{
  GtkWidget *expander;
  GtkGesture *gesture;
  GtkDragSource *drag;
  GtkDropTarget *drop;

  g_assert (BOOKUP_IS_SECTIONS_VIEW (self));
  g_assert (GTK_IS_LIST_ITEM (item));
  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));

  expander = gtk_tree_expander_new ();
  gtk_list_item_set_child (item, expander);

  gesture = gtk_gesture_click_new ();
  gtk_gesture_single_set_button (GTK_GESTURE_SINGLE (gesture), GDK_BUTTON_SECONDARY);

  g_signal_connect (gesture,
                    "pressed",
                    G_CALLBACK (sections_view_onButtonPressed),
                    item);

  gtk_widget_add_controller (GTK_WIDGET (expander), GTK_EVENT_CONTROLLER (gesture));

  g_signal_connect (expander, "destroy", G_CALLBACK (on_expander_destroy), NULL);

  /* Setup Drag gesture for this row */
  drag = gtk_drag_source_new ();
  gtk_drag_source_set_actions (drag, GDK_ACTION_COPY);
  g_signal_connect (drag, "prepare", G_CALLBACK (on_drag_prepare), NULL);
  g_signal_connect (drag, "drag-begin", G_CALLBACK (on_drag_begin), NULL);
  gtk_widget_add_controller (GTK_WIDGET (expander), GTK_EVENT_CONTROLLER (drag));

  /* Setup Drop gesture for this row */
  drop = gtk_drop_target_new (G_TYPE_INVALID, GDK_ACTION_COPY);
  gtk_drop_target_set_gtypes (drop, (GType[2]){
                                        GTK_TYPE_TREE_LIST_ROW,
                                        BOOKUP_TYPE_PAGE,
                                    },
                              2);
  g_signal_connect (drop, "drop", G_CALLBACK (bookup_sections_view_on_drop), self);
  gtk_widget_add_controller (expander, GTK_EVENT_CONTROLLER (drop));
}

static void
bind_list_cb (BookupSectionsView *self,
              GtkListItem *item,
              GtkSignalListItemFactory *factory)
{
  g_assert (BOOKUP_IS_SECTIONS_VIEW (self));
  g_assert (GTK_IS_LIST_ITEM (item));
  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));

  GtkTreeExpander *expander;
  GtkTreeListRow *row;
  BookupSection *section;
  GtkWidget *box;
  GtkWidget *label;
  BookupSectionIcon *icon;

  row = GTK_TREE_LIST_ROW (gtk_list_item_get_item (item));
  expander = GTK_TREE_EXPANDER (gtk_list_item_get_child (item));
  section = gtk_tree_list_row_get_item (row);

  g_assert (GTK_IS_TREE_LIST_ROW (row));
  g_assert (GTK_IS_TREE_EXPANDER (expander));
  g_assert (BOOKUP_IS_SECTION (section));

  gtk_tree_expander_set_list_row (expander, row);

  icon = g_object_new (BOOKUP_TYPE_SECTION_ICON,
                       "content-width", 16,
                       "content-height", 16,
                       "rgb", bookup_section_get_rgb (section),
                       NULL);

  box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
  label = gtk_label_new (bookup_section_get_name (section));
  g_object_bind_property (section, "name", label, "label", G_BINDING_DEFAULT);
  g_object_bind_property (section, "rgb", icon, "rgb", G_BINDING_DEFAULT);

  gtk_box_append (GTK_BOX (box), GTK_WIDGET (icon));
  gtk_box_append (GTK_BOX (box), label);
  gtk_list_item_set_selectable (item, FALSE);
  gtk_label_set_xalign (GTK_LABEL (label), 0);
  gtk_tree_expander_set_child (expander, box);
}

void
bookup_sections_view_select_section (BookupSectionsView *self, BookupSection *section)
{
  g_return_if_fail (BOOKUP_IS_SECTION (section));

  GListModel *model = G_LIST_MODEL (self->tree_model);
  unsigned int pos = 0;
  GtkTreeListRow *row;
  BookupSection *sibbling;
  BookupSectionsTree *tree = BOOKUP_SECTIONS_TREE (gtk_tree_list_model_get_model (self->tree_model));

  while ((row = g_list_model_get_item (model, pos)))
    {
      sibbling = gtk_tree_list_row_get_item (row);
      g_assert (BOOKUP_IS_SECTION (sibbling));

      if (bookup_sections_tree_section_is_ancestor (tree, sibbling, section))
        {
          g_debug ("%s is ancestor", bookup_section_get_name (sibbling));
          gtk_tree_list_row_set_expanded (row, TRUE);
        }

      if (bookup_section_get_id (sibbling) == bookup_section_get_id (section))
        {
          select_section_row (self, row);
          break;
        }

      pos++;
    }
}

void
bookup_sections_view_add (BookupSectionsView *self,
                          BookupSection *section)
{
  GtkTreeListRow *row;
  GListModel *model = NULL;
  row = gtk_single_selection_get_selected_item (self->selection);

  if (bookup_section_get_parent_id (section))
    {
      model = gtk_tree_list_row_get_children (row);

      if (!model && gtk_tree_list_row_get_parent (row))
        model = gtk_tree_list_row_get_children (gtk_tree_list_row_get_parent (row));
    }

  // Fallback to root model
  if (model == NULL)
    model = gtk_tree_list_model_get_model (self->tree_model);

  bookup_sections_tree_add_section (BOOKUP_SECTIONS_TREE (model), section);
}

static GListModel *
section_tree_create_child_model_cb (gpointer item,
                                    gpointer user_data)
{
  BookupSection *section = item;
  BookupSectionsView *self = user_data;

  g_return_val_if_fail (BOOKUP_IS_SECTION (section), NULL);
  g_return_val_if_fail (BOOKUP_IS_SECTIONS_VIEW (self), NULL);

  BookupSectionsTree *tree = BOOKUP_SECTIONS_TREE (gtk_tree_list_model_get_model (self->tree_model));

  if (bookup_sections_tree_section_has_children (tree, section))
    return G_LIST_MODEL (bookup_sections_tree_new (section, NULL));

  return NULL;
}

void
bookup_sections_view_set_model (BookupSectionsView *self, GListModel *list)
{
  BookupSectionsTree *tree = bookup_sections_tree_new (NULL, list);

  if (self->tree_model)
    g_object_unref (self->tree_model);

  self->tree_model = gtk_tree_list_model_new (g_object_ref (G_LIST_MODEL (tree)),
                                              FALSE, // Passthrough
                                              FALSE, // Autoexpand
                                              section_tree_create_child_model_cb,
                                              g_object_ref (self), NULL);

  gtk_single_selection_set_model (self->selection,
                                  G_LIST_MODEL (self->tree_model));
}

void
bookup_sections_view_remove_all (BookupSectionsView *self)
{
  BookupSectionsTree *tree;

  if (self->tree_model)
    {
      tree = BOOKUP_SECTIONS_TREE (gtk_tree_list_model_get_model (self->tree_model));
      bookup_sections_tree_remove_all (tree);
      g_object_unref (self->tree_model);
      self->tree_model = NULL;
      gtk_single_selection_set_model (self->selection, NULL);
    }
}

BookupSection *
bookup_sections_view_get_selected (BookupSectionsView *self)
{
  BookupSection *section = NULL;
  GtkTreeListRow *row;
  row = gtk_single_selection_get_selected_item (self->selection);
  section = gtk_tree_list_row_get_item (row);

  return section;
}

void
bookup_sections_view_remove_selected (BookupSectionsView *self)
{
  BookupSection *selected_section;
  GtkTreeListRow *row;
  GListModel *model;

  row = gtk_single_selection_get_selected_item (self->selection);

  GtkTreeListRow *parent = gtk_tree_list_row_get_parent (row);

  if (parent)
    {
      model = gtk_tree_list_row_get_children (parent);
    }
  else
    {
      // Get the root GListModel
      model = gtk_tree_list_model_get_model (self->tree_model);
    }

  selected_section = gtk_tree_list_row_get_item (row);

  bookup_sections_tree_remove (BOOKUP_SECTIONS_TREE (model), selected_section);
}

static void
on_section_activate_cb (BookupSectionsView *self, unsigned int position, GtkListView *listview)
{
  g_autoptr (BookupSection) section = NULL;

  g_assert (GTK_IS_LIST_VIEW (listview));

  GtkTreeListRow *row;

  row = gtk_tree_list_model_get_row (self->tree_model, position);
  section = gtk_tree_list_row_get_item (row);

  g_assert (BOOKUP_IS_SECTION (section));

  g_signal_emit (self, signals[ACTIVATE], 0, section);

  gtk_selection_model_select_item (
      GTK_SELECTION_MODEL (self->selection),
      position,
      TRUE);
}

static void
bookup_sections_view_class_init (BookupSectionsViewClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/gitlab/ilhooq/Bookup/bookup-sections-view.ui");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_bind_template_child (widget_class, BookupSectionsView, selection);
  gtk_widget_class_bind_template_child (widget_class, BookupSectionsView, menu);
  gtk_widget_class_bind_template_child (widget_class, BookupSectionsView, root_section);
  gtk_widget_class_bind_template_callback (widget_class, on_section_activate_cb);
  gtk_widget_class_bind_template_callback (widget_class, sections_view_onButtonPressed);
  gtk_widget_class_bind_template_callback (widget_class, setup_list_cb);
  gtk_widget_class_bind_template_callback (widget_class, bind_list_cb);

  signals[ACTIVATE] = g_signal_new ("activate",
                                    G_TYPE_FROM_CLASS (klass),
                                    G_SIGNAL_RUN_LAST,
                                    0,
                                    NULL, NULL,
                                    bookup_marshal_VOID__OBJECT,
                                    G_TYPE_NONE,
                                    1, BOOKUP_TYPE_SECTION);

  g_signal_set_va_marshaller (signals[ACTIVATE],
                              G_TYPE_FROM_CLASS (klass),
                              bookup_marshal_VOID__OBJECTv);

  signals[DROP_SECTION] = g_signal_new ("drop-section",
                                        G_TYPE_FROM_CLASS (klass),
                                        G_SIGNAL_RUN_LAST,
                                        0,
                                        NULL, NULL,
                                        bookup_marshal_VOID__INT_INT,
                                        G_TYPE_NONE,
                                        2, G_TYPE_INT, G_TYPE_INT);

  g_signal_set_va_marshaller (signals[DROP_SECTION],
                              G_TYPE_FROM_CLASS (klass),
                              bookup_marshal_VOID__INT_INTv);

  signals[DROP_PAGE] = g_signal_new ("drop-page",
                                     G_TYPE_FROM_CLASS (klass),
                                     G_SIGNAL_RUN_LAST,
                                     0,
                                     NULL, NULL,
                                     bookup_marshal_VOID__INT_INT,
                                     G_TYPE_NONE,
                                     2, G_TYPE_INT, G_TYPE_INT);

  g_signal_set_va_marshaller (signals[DROP_PAGE],
                              G_TYPE_FROM_CLASS (klass),
                              bookup_marshal_VOID__INT_INTv);
}

static void
bookup_sections_view_init (BookupSectionsView *self)
{
  GtkDropTarget *drop;

  self->tree_model = NULL;
  gtk_widget_init_template (GTK_WIDGET (self));

  /* Setup drop gesture on the widget representing the root section */
  drop = gtk_drop_target_new (G_TYPE_INVALID, GDK_ACTION_COPY);
  gtk_drop_target_set_gtypes (drop, (GType[1]){
                                        GTK_TYPE_TREE_LIST_ROW,
                                    },
                              1);

  g_signal_connect (drop, "drop", G_CALLBACK (bookup_sections_view_on_drop), self);
  gtk_widget_add_controller (GTK_WIDGET (self->root_section), GTK_EVENT_CONTROLLER (drop));
}
