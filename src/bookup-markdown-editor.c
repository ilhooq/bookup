/* bookup-markdown-editor.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-markdown-editor.h"
#include "bookup-marshal.h"
#include "utils.h"
#include <glib/gi18n.h>
#include <gtksourceview/gtksource.h>

enum
{
  FIRST_LINE_CHANGED,
  N_SIGNALS
};

struct _BookupMarkdownEditor
{
  GtkBox parent_instance;

  GSimpleActionGroup *actions;
  GtkSourceView *source_view;
  GtkSourceBuffer *source_buffer;
  gulong source_change_cb_id;
  gboolean content_changed;
  int fll; // first line length
};

G_DEFINE_FINAL_TYPE (BookupMarkdownEditor, bookup_markdown_editor, GTK_TYPE_BOX)

static unsigned int signals[N_SIGNALS];

enum
{
  PROP_STYLE_SCHEME = 1,
  N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

static void
dispose (GObject *obj)
{
  BookupMarkdownEditor *self = BOOKUP_MARKDOWN_EDITOR (obj);
  gtk_text_view_set_buffer (GTK_TEXT_VIEW (self->source_view), NULL);
  g_clear_object (&self->source_buffer);
  G_OBJECT_CLASS (bookup_markdown_editor_parent_class)->dispose (obj);
}

static void
source_change_cb (GtkTextBuffer *buffer, gpointer user_data)
{
  BookupMarkdownEditor *self = BOOKUP_MARKDOWN_EDITOR (user_data);
  int cursor_position;
  g_object_get (G_OBJECT (buffer),
                "cursor-position", &cursor_position,
                NULL);

  self->content_changed = TRUE;

  if (cursor_position - 1 <= self->fll)
    {
      GtkTextIter start;
      GtkTextIter end;
      char *first_line;

      gtk_text_buffer_get_start_iter (buffer, &start);
      gtk_text_buffer_get_iter_at_offset (buffer, &end, cursor_position);
      gtk_text_iter_forward_to_line_end (&end);
      first_line = gtk_text_buffer_get_text (buffer, &start, &end, TRUE);
      self->fll = strlen (first_line);
      g_signal_emit (self, signals[FIRST_LINE_CHANGED], 0, first_line);
      g_free (first_line);
    }
}

void
bookup_markdown_editor_load_content (BookupMarkdownEditor *self,
                                     const gchar *content)
{
  if (self->source_change_cb_id)
    g_signal_handler_disconnect (self->source_buffer, self->source_change_cb_id);

  self->content_changed = false;
  // Undefined ref
  // gtk_source_buffer_begin_not_undoable_action(self->source_buffer);
  gtk_text_buffer_set_text (GTK_TEXT_BUFFER (self->source_buffer), content, -1);
  self->source_change_cb_id = g_signal_connect (self->source_buffer,
                                                "changed",
                                                G_CALLBACK (source_change_cb),
                                                (gpointer) self);

  // Undefined ref
  // gtk_source_buffer_end_not_undoable_action(self->source_buffer);
  char *first_line = get_first_line (content);
  self->fll = (first_line) ? strlen (first_line) : 0;
  g_free (first_line);
}

void
bookup_markdown_editor_empty (BookupMarkdownEditor *self)
{
  if (self->source_change_cb_id)
    g_signal_handler_disconnect (self->source_buffer, self->source_change_cb_id);
  // Undefined ref
  // gtk_source_buffer_begin_not_undoable_action(self->source_buffer);
  self->source_change_cb_id = 0;
  gtk_text_buffer_set_text (GTK_TEXT_BUFFER (self->source_buffer), "", -1);
}

gchar *
bookup_markdown_editor_get_content (BookupMarkdownEditor *self)
{
  GtkTextIter start, end;

  gtk_text_buffer_get_start_iter (GTK_TEXT_BUFFER (self->source_buffer), &start);
  gtk_text_buffer_get_end_iter (GTK_TEXT_BUFFER (self->source_buffer), &end);

  return gtk_text_buffer_get_text (GTK_TEXT_BUFFER (self->source_buffer),
                                   &start,
                                   &end,
                                   FALSE);
}

gboolean
bookup_markdown_editor_content_changed (BookupMarkdownEditor *self)
{
  return self->content_changed;
}

static void
on_format (GSimpleAction *action,
           GVariant *parameter,
           gpointer user_data)
{
  BookupMarkdownEditor *self;
  const char *type;
  const char *format;
  GtkTextBuffer *buffer;
  GtkTextIter iter, start, end;
  g_autofree char *new_text = NULL;
  int cursor_backward = 0;

  g_assert (G_IS_ACTION (action));
  g_assert (BOOKUP_IS_MARKDOWN_EDITOR (user_data));

  type = g_variant_get_string (parameter, NULL);

  self = BOOKUP_MARKDOWN_EDITOR (user_data);
  buffer = GTK_TEXT_BUFFER (self->source_buffer);

  if (g_str_equal (type, "bold"))
    {
      format = "**%s**";
      cursor_backward = 2;
    }
  else if (g_str_equal (type, "italic"))
    {
      format = "_%s_";
      cursor_backward = 1;
    }
  else if (g_str_equal (type, "strikethrough"))
    {
      format = "~~%s~~";
      cursor_backward = 2;
    }
  else if (g_str_equal (type, "code"))
    {
      format = "`%s`";
      cursor_backward = 1;
    }
  else if (g_str_equal (type, "heading"))
    format = "## %s";
  else if (g_str_equal (type, "heading-1"))
    format = "# %s";
  else if (g_str_equal (type, "heading-3"))
    format = "### %s";
  else if (g_str_equal (type, "separator"))
    format = "\n\n-----\n\n%s";
  else if (g_str_equal (type, "table"))
    format = "\n| Header | Header |\n"
             "| ------ | ------ |\n"
             "| Text %s | Text |\n"
             "| Text | Text |\n";
  else if (g_str_equal (type, "unordered-list"))
    format = "- %s";
  else if (g_str_equal (type, "ordered-list"))
    format = "1. %s";
  else if (g_str_equal (type, "quote"))
    format = "> %s";
  else if (g_str_equal (type, "referal"))
    format = "%s [^1]";
  else if (g_str_equal (type, "footnote"))
    format = "[^1]: %s";
  else if (g_str_equal (type, "link"))
    {
      format = "[%s]()";
      cursor_backward = 1;
    }
  else
    {
      g_warning ("Unknown format type : %s", type);

      return;
    }

  if (gtk_text_buffer_get_selection_bounds (buffer, &start, &end))
    {
      /* We have a selected text */
      g_autofree char *selected_text = NULL;

      selected_text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
      new_text = g_strdup_printf (format, selected_text);
      gtk_text_buffer_delete (buffer, &start, &end);
      gtk_text_buffer_insert (buffer, &start, new_text, -1);

      goto focus;
    }

  if (g_str_equal (type, "link"))
    {
      new_text = g_strdup_printf ("[%s](%s)", _("Text"), _("Link"));
    }
  else
    {
      new_text = g_strdup_printf (format, "");
    }

  gtk_text_buffer_insert_at_cursor (buffer, new_text, -1);

focus:
  /* Update the iter at the cursor position */
  gtk_text_buffer_get_iter_at_mark (buffer, &iter, gtk_text_buffer_get_insert (buffer));

  if (cursor_backward > 0)
    /* Move the cursor between markups */
    gtk_text_iter_backward_chars (&iter, cursor_backward);

  gtk_text_buffer_place_cursor (buffer, &iter);
  gtk_widget_grab_focus (GTK_WIDGET (self->source_view));
}

static const char *
bookup_markdown_editor_get_style_scheme (BookupMarkdownEditor *self)
{
  GtkSourceStyleScheme *style_scheme;
  style_scheme = gtk_source_buffer_get_style_scheme (self->source_buffer);

  return gtk_source_style_scheme_get_id (style_scheme);
}

void
bookup_markdown_editor_set_style_scheme (BookupMarkdownEditor *self,
                                         const char *style_scheme_id)
{
  GtkSourceStyleSchemeManager *style_scheme_manager;
  GtkSourceStyleScheme *style_scheme;

  g_return_if_fail (BOOKUP_IS_MARKDOWN_EDITOR (self));

  style_scheme_manager = gtk_source_style_scheme_manager_get_default ();
  style_scheme = gtk_source_style_scheme_manager_get_scheme (style_scheme_manager, style_scheme_id);

  // Fallback to Adwaita if we don't find a match
  if (gtk_source_style_scheme_manager_get_scheme (style_scheme_manager, style_scheme_id) == NULL)
    style_scheme_id = "Adwaita";

  gtk_source_buffer_set_style_scheme (self->source_buffer, style_scheme);
}

void
bookup_markdown_editor_insert_image (BookupMarkdownEditor *self, unsigned int id)
{
  g_autofree char *markdown_code = NULL;
  markdown_code = g_strdup_printf ("<image %u>", id);

  gtk_text_buffer_insert_at_cursor (
      GTK_TEXT_BUFFER (self->source_buffer),
      markdown_code,
      -1);
}

static void
bookup_markdown_editor_get_property (GObject *object,
                                     guint prop_id,
                                     GValue *value,
                                     GParamSpec *pspec)
{
  BookupMarkdownEditor *self = BOOKUP_MARKDOWN_EDITOR (object);

  switch (prop_id)
    {
    case PROP_STYLE_SCHEME:
      g_value_set_string (value, bookup_markdown_editor_get_style_scheme (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bookup_markdown_editor_set_property (GObject *object,
                                     guint prop_id,
                                     const GValue *value,
                                     GParamSpec *pspec)
{
  BookupMarkdownEditor *self = BOOKUP_MARKDOWN_EDITOR (object);

  switch (prop_id)
    {
    case PROP_STYLE_SCHEME:
      bookup_markdown_editor_set_style_scheme (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bookup_markdown_editor_class_init (BookupMarkdownEditorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->dispose = dispose;
  object_class->get_property = bookup_markdown_editor_get_property;
  object_class->set_property = bookup_markdown_editor_set_property;

  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/gitlab/ilhooq/Bookup/bookup-markdown-editor.ui");
  gtk_widget_class_bind_template_child (widget_class, BookupMarkdownEditor, source_view);

  properties[PROP_STYLE_SCHEME] =
      g_param_spec_string ("style-scheme",
                           "Style Scheme",
                           "The style scheme for the editor",
                           NULL,
                           (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPERTIES, properties);

  signals[FIRST_LINE_CHANGED] = g_signal_new ("first-line-changed",
                                              G_TYPE_FROM_CLASS (klass),
                                              G_SIGNAL_RUN_LAST,
                                              0,
                                              NULL, NULL,
                                              bookup_marshal_VOID__STRING,
                                              G_TYPE_NONE,
                                              1, G_TYPE_STRING);

  g_signal_set_va_marshaller (signals[FIRST_LINE_CHANGED],
                              G_TYPE_FROM_CLASS (klass),
                              bookup_marshal_VOID__STRINGv);

  gtk_widget_class_add_binding_action (
      widget_class, GDK_KEY_B, GDK_CONTROL_MASK, "editor.format", "s", "bold");
  gtk_widget_class_add_binding_action (
      widget_class, GDK_KEY_I, GDK_CONTROL_MASK, "editor.format", "s", "italic");
  gtk_widget_class_add_binding_action (
      widget_class, GDK_KEY_T, GDK_CONTROL_MASK, "editor.format", "s", "strikethrough");
}

static const GActionEntry entries[] = {
  { .name = "format", .activate = on_format, .parameter_type = "s" },
};

static void
bookup_markdown_editor_init (BookupMarkdownEditor *self)
{
  GtkSourceLanguageManager *manager;
  GtkSourceLanguage *language;

  self->source_change_cb_id = 0;
  self->content_changed = FALSE;
  self->fll = 0;
  self->source_buffer = gtk_source_buffer_new (NULL);

  gtk_widget_init_template (GTK_WIDGET (self));

  gtk_text_view_set_buffer (GTK_TEXT_VIEW (self->source_view),
                            GTK_TEXT_BUFFER (self->source_buffer));

  manager = gtk_source_language_manager_get_default ();
  language = gtk_source_language_manager_get_language (manager, "markdown");

  self->actions = g_simple_action_group_new ();

  gtk_widget_insert_action_group (GTK_WIDGET (self), "editor", G_ACTION_GROUP (self->actions));

  gtk_source_buffer_set_language (self->source_buffer, language);

  g_action_map_add_action_entries (G_ACTION_MAP (self->actions),
                                   entries,
                                   G_N_ELEMENTS (entries),
                                   self);
}
