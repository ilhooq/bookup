/* bookup-section-dialog.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-section-dialog.h"
#include "bookup-marshal.h"

enum
{
  RESPONSE,
  N_SIGNALS
};

struct _BookupSectionDialog
{
  GtkWindow parent_instance;

  GtkEntry *section_name;
  GtkColorDialogButton *section_color;
  BookupSection *section;
};

G_DEFINE_FINAL_TYPE (BookupSectionDialog, bookup_section_dialog, GTK_TYPE_WINDOW)

static unsigned int signals[N_SIGNALS];

static void
on_cancel (BookupSectionDialog *self)
{
  g_return_if_fail (BOOKUP_IS_SECTION_DIALOG (self));
  g_signal_emit (self, signals[RESPONSE], 0, GTK_RESPONSE_CANCEL);
}

static void
on_save (BookupSectionDialog *self)
{
  const char *name;
  const GdkRGBA *rgba;
  g_autofree char *rgb = NULL;

  g_return_if_fail (BOOKUP_IS_SECTION_DIALOG (self));

  name = gtk_editable_get_text (GTK_EDITABLE (self->section_name));
  rgba = gtk_color_dialog_button_get_rgba (self->section_color);
  rgb = gdk_rgba_to_string (rgba);

  bookup_section_set_name (self->section, name);
  bookup_section_set_rgb (self->section, rgb);

  g_signal_emit (self, signals[RESPONSE], 0, GTK_RESPONSE_OK);
}

static void
bookup_section_dialog_class_init (BookupSectionDialogClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/gitlab/ilhooq/Bookup/bookup-section-dialog.ui");
  gtk_widget_class_bind_template_child (widget_class, BookupSectionDialog, section_name);
  gtk_widget_class_bind_template_child (widget_class, BookupSectionDialog, section_color);
  gtk_widget_class_bind_template_callback (widget_class, on_cancel);
  gtk_widget_class_bind_template_callback (widget_class, on_save);

  signals[RESPONSE] = g_signal_new ("response",
                                    G_TYPE_FROM_CLASS (klass),
                                    G_SIGNAL_RUN_LAST,
                                    0,
                                    NULL, NULL,
                                    bookup_marshal_VOID__INT,
                                    G_TYPE_NONE,
                                    1, G_TYPE_INT);

  g_signal_set_va_marshaller (signals[RESPONSE],
                              G_TYPE_FROM_CLASS (klass),
                              bookup_marshal_VOID__INTv);
}

static void
bookup_section_dialog_init (BookupSectionDialog *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->section = g_object_new (BOOKUP_TYPE_SECTION, NULL);
}

void
bookup_section_dialog_set_section (BookupSectionDialog *self,
                                   BookupSection *section)
{
  GdkRGBA color;

  self->section = BOOKUP_SECTION (section);
  gtk_editable_set_text (GTK_EDITABLE (self->section_name), bookup_section_get_name (section));
  gdk_rgba_parse (&color, bookup_section_get_rgb (section));

  gtk_color_dialog_button_set_rgba (self->section_color, &color);
}

BookupSection *
bookup_section_dialog_get_section (BookupSectionDialog *self)
{
  return self->section;
}
