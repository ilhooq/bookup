/* bookup-page.h
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BOOKUP_TYPE_PAGE (bookup_page_get_type ())

G_DECLARE_FINAL_TYPE (BookupPage, bookup_page, BOOKUP, PAGE, GObject)

BookupPage *bookup_page_new ();

void bookup_page_set_id (BookupPage *self, unsigned int id);
void bookup_page_set_name (BookupPage *self, char *name);
void bookup_page_set_data (BookupPage *self, char *data);
void bookup_page_set_html_cache (BookupPage *self, char *html_cache);
void bookup_page_set_creation_date (BookupPage *self, unsigned int creation_date);
void bookup_page_set_modification_date (BookupPage *self, unsigned int modification_date);
void bookup_page_set_section_id (BookupPage *self, unsigned int section_id);
void bookup_page_set_order_num (BookupPage *self, unsigned int order_num);
void bookup_page_set_starred (BookupPage *page, gboolean starred);

unsigned int bookup_page_get_id (BookupPage *self);
char *bookup_page_get_name (BookupPage *self);
char *bookup_page_get_data (BookupPage *self);
char *bookup_page_get_html_cache (BookupPage *self);
unsigned int bookup_page_get_creation_date (BookupPage *self);
unsigned int bookup_page_get_modification_date (BookupPage *self);
unsigned int bookup_page_get_section_id (BookupPage *self);
unsigned int bookup_page_get_order_num (BookupPage *self);
gboolean bookup_page_get_starred (BookupPage *page);

G_END_DECLS

