/* bookup-pages-view.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-pages-view.h"
#include "bookup-marshal.h"
#include "bookup-page.h"
#include "bookup-section.h"
#include <glib/gi18n.h>

enum
{
  ACTIVATE,
  N_SIGNALS
};

struct _BookupPagesView
{
  GtkBox parent_instance;
  GtkLabel *pages_title;
  GBinding *bind_section_name;
  GtkButton *btn_new_page;
  GtkButton *btn_delete_page;
  BookupSection *section;
  GListModel *model;
  GtkSingleSelection *selection;
};

G_DEFINE_FINAL_TYPE (BookupPagesView, bookup_pages_view, GTK_TYPE_BOX)

static unsigned int signals[N_SIGNALS];

static GdkContentProvider *
on_drag_prepare (G_GNUC_UNUSED GtkDragSource *source,
                 G_GNUC_UNUSED double x,
                 G_GNUC_UNUSED double y,
                 GtkListItem *item)
{
  BookupPage *page;

  g_assert (GTK_IS_LIST_ITEM (item));
  page = gtk_list_item_get_item (item);
  g_assert (BOOKUP_IS_PAGE (page));

  return gdk_content_provider_new_typed (BOOKUP_TYPE_PAGE, page);
}

static void
on_drag_begin (GtkDragSource *source,
               GdkDrag *drag,
               G_GNUC_UNUSED gpointer user_data)
{
  g_autoptr (GdkPaintable) paintable = NULL;
  GtkWidget *widget;

  g_assert (GDK_IS_DRAG (drag));
  g_assert (GTK_IS_DRAG_SOURCE (source));

  /* Get our Label */
  widget = gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (source));

  /* But snapshot the parent to get row content */
  widget = gtk_widget_get_parent (widget);

  if ((paintable = gtk_widget_paintable_new (widget)))
    {
      GtkSnapshot *snapshot = gtk_snapshot_new ();
      double width = gdk_paintable_get_intrinsic_width (paintable);
      double height = gdk_paintable_get_intrinsic_height (paintable);
      g_autoptr (GdkPaintable) with_parent = NULL;

      gdk_paintable_snapshot (paintable, snapshot, width, height);

      with_parent = gtk_snapshot_free_to_paintable (snapshot, &GRAPHENE_SIZE_INIT (width, height));
      gtk_drag_source_set_icon (source, paintable, 0, 0);
    }
}

static void
setup_list_cb (BookupPagesView *self,
               GtkListItem *item,
               GtkSignalListItemFactory *factory)
{
  GtkDragSource *drag;
  GtkWidget *label;

  g_assert (BOOKUP_IS_PAGES_VIEW (self));
  g_assert (GTK_IS_LIST_ITEM (item));
  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));

  label = gtk_label_new ("");
  gtk_list_item_set_child (item, label);

  /* Setup Drop gesture for this row */
  drag = gtk_drag_source_new ();
  gtk_drag_source_set_actions (drag, GDK_ACTION_COPY);
  g_signal_connect (drag, "prepare", G_CALLBACK (on_drag_prepare), item);
  g_signal_connect (drag, "drag-begin", G_CALLBACK (on_drag_begin), NULL);
  gtk_widget_add_controller (label, GTK_EVENT_CONTROLLER (drag));
}

static void
bind_list_cb (BookupPagesView *self,
              GtkListItem *item,
              GtkSignalListItemFactory *factory)
{
  GtkWidget *label;
  BookupPage *page;

  g_assert (BOOKUP_IS_PAGES_VIEW (self));
  g_assert (GTK_IS_LIST_ITEM (item));
  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));

  gtk_list_item_set_selectable (item, FALSE);
  label = gtk_list_item_get_child (item);
  page = gtk_list_item_get_item (item);
  gtk_label_set_xalign (GTK_LABEL (label), 0);
  gtk_label_set_label (GTK_LABEL (label), bookup_page_get_name (page));
  g_object_bind_property (page, "name", label, "label", G_BINDING_DEFAULT);
}

static gboolean
pages_have_same_id (gconstpointer item1,
                    gconstpointer item2)
{
  unsigned int id1 = bookup_page_get_id ((BookupPage *) item1);
  unsigned int id2 = bookup_page_get_id ((BookupPage *) item2);

  return id1 == id2;
}

static void
activate_cb (BookupPagesView *self, unsigned int position, GtkListView *listview)
{
  g_autoptr (BookupPage) page = NULL;

  g_return_if_fail (BOOKUP_IS_PAGES_VIEW (self));
  g_return_if_fail (GTK_IS_LIST_VIEW (listview));

  page = g_list_model_get_item (self->model, position);
  g_assert (BOOKUP_IS_PAGE (page));

  g_signal_emit (self, signals[ACTIVATE], 0, page);

  gtk_selection_model_select_item (
      GTK_SELECTION_MODEL (self->selection),
      position,
      TRUE);
}

static void
unbind_section_name (BookupPagesView *self)
{
  if (self->bind_section_name)
    {
      g_object_unref (self->bind_section_name);
      self->bind_section_name = NULL;
    }
}

void
bookup_pages_view_add_page (BookupPagesView *self, BookupPage *page)
{
  g_list_store_append (G_LIST_STORE (self->model), page);
}

void
bookup_pages_view_delete_page (BookupPagesView *self, BookupPage *page)
{
  unsigned int pos;

  g_return_if_fail (BOOKUP_IS_PAGES_VIEW (self));

  g_list_store_find_with_equal_func (G_LIST_STORE (self->model),
                                     page,
                                     pages_have_same_id,
                                     &pos);
  g_list_store_remove (G_LIST_STORE (self->model), pos);
}

BookupPage *
bookup_pages_view_get_selected_page (BookupPagesView *self)
{
  return gtk_single_selection_get_selected_item (self->selection);
}

void
bookup_pages_view_select_page (BookupPagesView *self, BookupPage *page)
{
  unsigned int position;
  g_list_store_find_with_equal_func (G_LIST_STORE (self->model),
                                     page,
                                     pages_have_same_id,
                                     &position);
  gtk_selection_model_select_item (
      GTK_SELECTION_MODEL (self->selection),
      position,
      TRUE);
}

void
bookup_pages_view_update_page (BookupPagesView *self, BookupPage *page)
{
  unsigned int position;
  BookupPage *page_item;

  g_list_store_find_with_equal_func (G_LIST_STORE (self->model),
                                     page,
                                     pages_have_same_id,
                                     &position);

  page_item = g_list_model_get_item (self->model, position);

  if (BOOKUP_IS_PAGE (page_item))
    bookup_page_set_name (page_item, bookup_page_get_name (page));
}

void
bookup_pages_view_load_section (BookupPagesView *self, BookupSection *section, GListModel *model)
{
  gtk_widget_set_sensitive (GTK_WIDGET (self->btn_new_page), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (self->btn_delete_page), TRUE);

  self->section = section;

  if (self->model)
    g_object_unref (self->model);

  self->model = model;

  gtk_label_set_label (self->pages_title, bookup_section_get_name (self->section));
  self->bind_section_name = g_object_bind_property (
      self->section, "name",
      self->pages_title, "label",
      G_BINDING_DEFAULT);
  gtk_single_selection_set_model (self->selection, self->model);
}

void
bookup_pages_view_remove_all (BookupPagesView *self)
{
  gtk_label_set_label (self->pages_title, "");
  unbind_section_name (self);

  if (self->model)
    {
      g_object_unref (self->model);
      self->model = NULL;
      gtk_single_selection_set_model (self->selection, self->model);
    }
}

void
bookup_pages_view_load_search (BookupPagesView *self, const char *search, GListModel *model)
{
  g_autofree char *title = NULL;

  unbind_section_name (self);

  gtk_widget_set_sensitive (GTK_WIDGET (self->btn_new_page), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (self->btn_delete_page), TRUE);

  if (self->model)
    g_object_unref (self->model);

  self->model = model;

  title = g_strdup_printf ("%s : %s", _("Search"), search);
  gtk_label_set_label (self->pages_title, title);

  gtk_single_selection_set_model (self->selection, self->model);
}

void
bookup_pages_view_load_tag (BookupPagesView *self, const char *tag, GListModel *model)
{
  g_autofree char *title = NULL;

  unbind_section_name (self);

  gtk_widget_set_sensitive (GTK_WIDGET (self->btn_new_page), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (self->btn_delete_page), TRUE);

  title = g_strdup_printf ("%s : %s", _("Tag"), tag);
  gtk_label_set_label (self->pages_title, title);

  if (self->model)
    g_object_unref (self->model);

  self->model = model;
  gtk_single_selection_set_model (self->selection, self->model);
}

static void
bookup_pages_view_class_init (BookupPagesViewClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/gitlab/ilhooq/Bookup/bookup-pages-view.ui");
  gtk_widget_class_bind_template_child (widget_class, BookupPagesView, pages_title);
  gtk_widget_class_bind_template_child (widget_class, BookupPagesView, btn_new_page);
  gtk_widget_class_bind_template_child (widget_class, BookupPagesView, btn_delete_page);
  gtk_widget_class_bind_template_child (widget_class, BookupPagesView, selection);
  gtk_widget_class_bind_template_callback (widget_class, activate_cb);
  gtk_widget_class_bind_template_callback (widget_class, setup_list_cb);
  gtk_widget_class_bind_template_callback (widget_class, bind_list_cb);

  signals[ACTIVATE] = g_signal_new ("activate",
                                    G_TYPE_FROM_CLASS (klass),
                                    G_SIGNAL_RUN_LAST,
                                    0,
                                    NULL, NULL,
                                    bookup_marshal_VOID__OBJECT,
                                    G_TYPE_NONE,
                                    1, BOOKUP_TYPE_PAGE);

  g_signal_set_va_marshaller (signals[ACTIVATE],
                              G_TYPE_FROM_CLASS (klass),
                              bookup_marshal_VOID__OBJECTv);
}

static void
bookup_pages_view_init (BookupPagesView *self)
{
  self->section = NULL;
  self->model = NULL;
  self->pages_title = NULL;
  self->bind_section_name = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));
}
