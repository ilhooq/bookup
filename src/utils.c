/* utils.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "utils.h"
#include "config.h"
#include <mkdio.h>

/*
 * Convert markdown string to HTML
 * See http://www.pell.portland.or.us/~orc/Code/discount/
 */
#if LIBMARKDOWN_VERSION == 2
#define MARKDOWN_FLAGS MKD_NOSTYLE | MKD_GITHUBTAGS | MKD_FENCEDCODE | MKD_DLEXTRA | MKD_EXTRA_FOOTNOTE | MKD_AUTOLINK
#endif

char *
markdown_to_html (const char *markdown)
{
  int size;
  char *html;
  char *output;
  MMIOT *md_doc;

  size = strlen (markdown);

#if LIBMARKDOWN_VERSION == 3
  mkd_flag_t *flags = mkd_flags ();
  mkd_set_flag_num (flags, MKD_NOSTYLE);
  mkd_set_flag_num (flags, MKD_GITHUBTAGS);
  mkd_set_flag_num (flags, MKD_FENCEDCODE);
  mkd_set_flag_num (flags, MKD_DLEXTRA);
  mkd_set_flag_num (flags, MKD_EXTRA_FOOTNOTE);
  mkd_set_flag_num (flags, MKD_AUTOLINK);
  md_doc = mkd_string (markdown, size, flags);
  mkd_compile (md_doc, flags);
#else
  md_doc = mkd_string (markdown, size, MARKDOWN_FLAGS);
  mkd_compile (md_doc, MARKDOWN_FLAGS);
#endif

  mkd_document (md_doc, &html);

  output = g_strdup (html);

  mkd_cleanup (md_doc);

#if LIBMARKDOWN_VERSION == 3
  mkd_free_flags (flags);
#endif

  return output;
}

char *
get_first_line (const char *string)
{
  GRegex *regex;
  GMatchInfo *match_info;
  char *ret_val = NULL;

  regex = g_regex_new ("^(\\s*.*)\\n*", G_REGEX_MULTILINE, 0, NULL);

  if (g_regex_match (regex, string, 0, &match_info))
    ret_val = g_match_info_fetch (match_info, 1);

  g_match_info_free (match_info);
  g_regex_unref (regex);

  return ret_val;
}

char *
extract_title (const char *string)
{
  GRegex *regex;
  GMatchInfo *match_info;
  char *ret_val = NULL;

  regex = g_regex_new ("^\\s*#* *(.*)\\n*", G_REGEX_MULTILINE, 0, NULL);

  if (g_regex_match (regex, string, 0, &match_info))
    ret_val = g_match_info_fetch (match_info, 1);

  g_match_info_free (match_info);
  g_regex_unref (regex);

  return ret_val;
}

char *
font_description_to_css (const PangoFontDescription *font_desc)
{
  PangoFontMask mask;
  GString *str;

  g_return_val_if_fail (font_desc, NULL);

  str = g_string_new (NULL);

  mask = pango_font_description_get_set_fields (font_desc);

  if (mask & PANGO_FONT_MASK_FAMILY)
    {
      const char *family = pango_font_description_get_family (font_desc);
      g_string_append_printf (str, "font-family:%s;", family);
    }

  if (mask & PANGO_FONT_MASK_SIZE)
    {
      int font_size = pango_font_description_get_size (font_desc) / PANGO_SCALE;
      g_string_append_printf (str, "font-size:%dpt;", font_size);
    }

  if (mask & PANGO_FONT_MASK_WEIGHT)
    {
      int weight = pango_font_description_get_weight (font_desc);

      switch (weight)
        {
        case PANGO_WEIGHT_SEMILIGHT:
        case PANGO_WEIGHT_BOOK:
        case PANGO_WEIGHT_NORMAL:
          g_string_append (str, "font-weight:normal;");
          break;

        case PANGO_WEIGHT_BOLD:
          g_string_append (str, "font-weight:bold;");
          break;

        case PANGO_WEIGHT_THIN:
          g_string_append (str, "font-weight:100;");
          break;

        case PANGO_WEIGHT_ULTRALIGHT:
          g_string_append (str, "font-weight:200;");
          break;

        case PANGO_WEIGHT_LIGHT:
          g_string_append (str, "font-weight:300;");
          break;

        case PANGO_WEIGHT_MEDIUM:
          g_string_append (str, "font-weight:500;");
          break;

        case PANGO_WEIGHT_SEMIBOLD:
          g_string_append (str, "font-weight:600;");
          break;

        case PANGO_WEIGHT_ULTRABOLD:
          g_string_append (str, "font-weight:800;");
          break;

        case PANGO_WEIGHT_HEAVY:
        case PANGO_WEIGHT_ULTRAHEAVY:
          g_string_append (str, "font-weight:900;");
          break;
        }
    }

  return g_string_free (str, FALSE);
}
