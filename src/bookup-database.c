/* bookup-database.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-database.h"
#include <sqlite3.h>

struct _BookupDatabase
{
  GObject parent_instance;
  sqlite3 *db;
};

G_DEFINE_TYPE (BookupDatabase, bookup_database, G_TYPE_OBJECT)

static void
database_finalize (GObject *object)
{
  BookupDatabase *self = BOOKUP_DATABASE (object);

  sqlite3_close (self->db);

  g_debug ("Database closed");

  G_OBJECT_CLASS (bookup_database_parent_class)->finalize (object);
}

static void
bookup_database_class_init (BookupDatabaseClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  gobject_class->finalize = database_finalize;
}

static void
bookup_database_init (BookupDatabase *self)
{
  self->db = NULL;
}

static void
bookup_database_create_schema (BookupDatabase *self, const char *schema_path)
{
  g_autoptr (GBytes) bytes = NULL;
  g_autofree char *query = NULL;

  bytes = g_resources_lookup_data (schema_path,
                                   0,
                                   NULL);

  query = g_strdup (g_bytes_get_data (bytes, NULL));

  bookup_database_exec (self, query);
}

BookupDatabase *
bookup_database_new (const char *filename, const char *schema_path)
{
  BookupDatabase *self = g_object_new (BOOKUP_TYPE_DATABASE, NULL);

  int res;

  if (!strlen (filename))
    return NULL;

  res = sqlite3_open (filename, &self->db);

  if (res)
    {
      g_warning ("Error to open database %s (Code %i)", filename, res);
      return NULL;
    }

  if (schema_path)
    bookup_database_create_schema (self, schema_path);

  schema_path ? g_debug ("Database created :%s", filename) : g_debug ("Database opened : %s", filename);

  return self;
}

gboolean
bookup_database_exec (BookupDatabase *self, const char *query)
{
  char *errMsg = NULL;

  if (sqlite3_exec (self->db, query, NULL, NULL, &errMsg) == SQLITE_OK)
    return TRUE;

  g_warning ("Sql error : %s (query: %s)", errMsg, query);
  sqlite3_free (errMsg);

  return FALSE;
}

gboolean
bookup_database_exec_with_blob (BookupDatabase *self,
                                const char *query,
                                const guint8 *blob_data,
                                gsize blob_size)
{
  sqlite3_stmt *stmt = NULL;

  if (sqlite3_prepare_v2 (self->db, query, -1, &stmt, NULL) != SQLITE_OK)
    {
      g_warning ("Failed to prepare query: %s", sqlite3_errmsg (self->db));
      return FALSE;
    }

  sqlite3_bind_blob (stmt, 1, blob_data, blob_size, SQLITE_STATIC);

  if (sqlite3_step (stmt) != SQLITE_DONE)
    {
      g_warning ("Failed to execute query: %s", sqlite3_errmsg (self->db));
      return FALSE;
    }

  sqlite3_finalize (stmt);

  return TRUE;
}

static int
count_row_callback (void *data,
                    G_GNUC_UNUSED int argc,
                    G_GNUC_UNUSED char **argv,
                    G_GNUC_UNUSED char **col)
{
  int *count = (int *) data;
  *count += 1;

  return 0;
}

GListModel *
bookup_database_fetch_rows (BookupDatabase *self,
                            const char *sql,
                            GType type)
{
  int rc;
  sqlite3_stmt *stmt;
  GListStore *store = g_list_store_new (type);

  rc = sqlite3_prepare_v2 (self->db, sql, -1, &stmt, 0);

  if (rc != SQLITE_OK)
    {
      g_warning ("Failed to fetch data. Err code: %i Err msg: %s",
                 rc, sqlite3_errmsg (self->db));
      goto end;
    }

  while (sqlite3_step (stmt) == SQLITE_ROW)
    {
      int count = sqlite3_column_count (stmt);
      GParamSpec *pspec;
      GObject *item = g_object_new (type, NULL);

      if (item == NULL)
        {
          g_warning ("Could not create an instance of type %s", g_type_name (type));
          break;
        }

      for (int i = 0; i < count; i++)
        {
          const char *col_name = sqlite3_column_name (stmt, i);
          int col_type = sqlite3_column_type (stmt, i);
          pspec = g_object_class_find_property (G_OBJECT_GET_CLASS (item), col_name);

          if (pspec)
            {
              GValue value = G_VALUE_INIT;

              if (G_IS_PARAM_SPEC_STRING (pspec))
                {
                  g_value_init (&value, G_TYPE_STRING);
                  g_value_set_string (&value, (gchar *) sqlite3_column_text (stmt, i));
                }
              else if (G_IS_PARAM_SPEC_UINT (pspec))
                {
                  g_value_init (&value, G_TYPE_UINT);
                  g_value_set_uint (&value, sqlite3_column_int (stmt, i));
                }
              else if (G_IS_PARAM_SPEC_INT (pspec))
                {
                  g_value_init (&value, G_TYPE_INT);
                  g_value_set_int (&value, sqlite3_column_int (stmt, i));
                }
              else if (G_IS_PARAM_SPEC_BOOLEAN (pspec))
                {
                  g_value_init (&value, G_TYPE_BOOLEAN);
                  g_value_set_boolean (&value, (gboolean) sqlite3_column_int (stmt, i));
                }
              else if (G_IS_PARAM_SPEC_POINTER (pspec) && col_type == SQLITE_BLOB)
                {
                  g_value_init (&value, G_TYPE_POINTER);
                  gsize len = sqlite3_column_bytes (stmt, i);
                  const void *src = sqlite3_column_blob (stmt, i);
                  GBytes *bytes = g_bytes_new (src, len);
                  g_value_set_pointer (&value, (gpointer) bytes);
                }

              else
                {
                  g_error ("Invalid property type for %s", col_name);
                }
              g_object_set_property (item, col_name, &value);
              g_value_unset (&value);
            }
        }

      g_list_store_append (store, item);
    }

end:
  sqlite3_finalize (stmt);

  return G_LIST_MODEL (store);
}

GObject *
bookup_database_fetch_row (BookupDatabase *db,
                           const char *sql,
                           GType type)
{
  g_autoptr (GListModel) list = NULL;
  GObject *row = NULL;

  list = bookup_database_fetch_rows (db, sql, type);

  if (g_list_model_get_n_items (list))
    row = g_object_ref (g_list_model_get_item (list, 0));

  return row;
}

char *
bookup_database_escape_string (const char *string)
{
  char *escaped, *sqlite_escaped;
  sqlite_escaped = sqlite3_mprintf ("%q", string);
  escaped = g_strdup (sqlite_escaped);
  sqlite3_free (sqlite_escaped);

  return escaped;
}

const char *
bookup_database_get_filename (BookupDatabase *self)
{
  return sqlite3_db_filename (self->db, NULL);
}

static char *
get_strescaped (GValue *value)
{
  char *escaped = NULL;

  switch (G_VALUE_TYPE (value))
    {
    case G_TYPE_BOOLEAN:
      escaped = sqlite3_mprintf ("%d", g_value_get_boolean (value));
      break;
    case G_TYPE_INT:
      escaped = sqlite3_mprintf ("%d", g_value_get_int (value));
      break;
    case G_TYPE_UINT:
      escaped = sqlite3_mprintf ("%d", g_value_get_uint (value));
      break;
    case G_TYPE_STRING:
      escaped = sqlite3_mprintf ("%q", g_value_get_string (value));
      break;
    }

  return escaped;
}

static gboolean
database_update (BookupDatabase *self,
                 char *table,
                 GObject *object)
{
  GParamSpec **properties;
  unsigned int i, id, n_properties;
  GString *query_string = g_string_new (NULL);
  g_autofree char *query = NULL;

  g_object_get (object, "id", &id, NULL);

  properties = g_object_class_list_properties (G_OBJECT_GET_CLASS (object), &n_properties);

  g_string_append_printf (query_string, "UPDATE %s SET ", table);

  for (i = 0; i < n_properties; ++i)
    {
      const char *name = g_param_spec_get_name (properties[i]);
      g_autofree char *column_name = g_strdelimit (g_strdup (name), "-", '_');

      GValue value = G_VALUE_INIT;
      char *escaped_name = NULL;
      char *escaped_value = NULL;

      if (g_str_equal (column_name, "id"))
        continue;

      g_value_init (&value, G_PARAM_SPEC_VALUE_TYPE (properties[i]));
      g_object_get_property (object, name, &value);

      escaped_name = sqlite3_mprintf ("%q", column_name);
      escaped_value = get_strescaped (&value);

      g_string_append_printf (query_string, "%s='%s'", escaped_name, escaped_value);

      if (i < n_properties - 1)
        g_string_append (query_string, ",");

      sqlite3_free (escaped_name);
      sqlite3_free (escaped_value);
      g_value_unset (&value);
    }

  g_string_append_printf (query_string, " WHERE id = %d", id);

  g_free (properties);
  query = g_string_free_and_steal (query_string);
  g_debug ("%s", query);

  return bookup_database_exec (self, query);
}

static gboolean
database_insert (BookupDatabase *self,
                 char *table,
                 GObject *object)
{
  GParamSpec **properties;
  unsigned int i, n_properties;
  GString *query_string = g_string_new (NULL);
  g_autofree char *query = NULL;

  properties = g_object_class_list_properties (G_OBJECT_GET_CLASS (object), &n_properties);

  g_string_append_printf (query_string, "INSERT INTO %s (", table);

  for (i = 0; i < n_properties; ++i)
    {
      const char *name = g_param_spec_get_name (properties[i]);

      g_autofree char *column_name = g_strdelimit (g_strdup (name), "-", '_');

      if (g_str_equal (column_name, "id"))
        continue;

      char *escaped_name = sqlite3_mprintf ("%q", column_name);
      g_string_append (query_string, escaped_name);

      if (i < n_properties - 1)
        g_string_append (query_string, ",");

      sqlite3_free (escaped_name);
    }

  g_string_append (query_string, ") VALUES (");

  for (i = 0; i < n_properties; ++i)
    {
      GValue value = G_VALUE_INIT;
      char *escaped_value = NULL;
      const char *name = g_param_spec_get_name (properties[i]);

      if (g_str_equal (name, "id"))
        continue;

      g_value_init (&value, G_PARAM_SPEC_VALUE_TYPE (properties[i]));
      g_object_get_property (object, name, &value);

      escaped_value = get_strescaped (&value);

      g_string_append_printf (query_string, "'%s'", escaped_value);

      if (i < n_properties - 1)
        g_string_append (query_string, ",");

      sqlite3_free (escaped_value);
      g_value_unset (&value);
    }

  g_string_append (query_string, ")");
  g_free (properties);

  query = g_string_free_and_steal (query_string);
  g_debug ("%s", query);

  if (bookup_database_exec (self, query))
    {
      int id = sqlite3_last_insert_rowid (self->db);
      g_object_set (object, "id", id, NULL);

      return TRUE;
    }

  return FALSE;
}

int
bookup_database_get_last_insert_id (BookupDatabase *self)
{
  return sqlite3_last_insert_rowid (self->db);
}

int
bookup_database_count_rows (BookupDatabase *self, const char *query)
{
  char *errmsg = NULL;
  int count = 0;

  int rc = sqlite3_exec (self->db, query, count_row_callback, (void *) &count, &errmsg);

  if (rc != SQLITE_OK)
    {
      g_warning ("Sql error : %s (query: %s)", errmsg, query);
    }

  sqlite3_free (errmsg);

  return count;
}

gboolean
bookup_database_save (BookupDatabase *db, char *table, GObject *object)
{
  GParamSpec *pspec;
  unsigned int id;

  pspec = g_object_class_find_property (G_OBJECT_GET_CLASS (object), "id");

  g_return_val_if_fail ((pspec != NULL), FALSE);

  g_object_get (object, "id", &id, NULL);

  if (id == 0)
    {
      return database_insert (db, table, object);
    }

  return database_update (db, table, object);
}

gboolean
bookup_database_delete (BookupDatabase *self, char *table, GObject *object)
{
  GParamSpec *pspec;
  unsigned int id;
  g_autofree char *query = NULL;

  pspec = g_object_class_find_property (G_OBJECT_GET_CLASS (object), "id");

  g_return_val_if_fail ((pspec != NULL), FALSE);

  g_object_get (object, "id", &id, NULL);

  g_return_val_if_fail ((id > 0), FALSE);

  query = g_strdup_printf ("DELETE FROM %s WHERE id = %d", table, id);

  return bookup_database_exec (self, query);
}
