#include <glib.h>
#include "utils.h"

static void test_get_first_line(void)
{
  char *testbuff = NULL;

  testbuff = get_first_line("Hello\nWorld!");
  g_assert_cmpstr(testbuff, ==, "Hello");
  g_free(testbuff);

  testbuff = get_first_line(" \tHello\nWorld!");
  g_assert_cmpstr(testbuff, ==, " \tHello");
  g_free(testbuff);
}

static void test_extract_title(void)
{
  char *testbuff = NULL;

  testbuff = extract_title("Hello\nWorld!");
  g_assert_cmpstr(testbuff, ==, "Hello");
  g_free(testbuff);

  testbuff = extract_title(" \t# Hello\nWorld!");
  g_assert_cmpstr(testbuff, ==, "Hello");
  g_free(testbuff);
}

int
main (int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Utils/get_first_line", test_get_first_line);
  g_test_add_func ("/Utils/extract_title", test_extract_title);
  return g_test_run ();
}

