#include "bookup-section.h"

static void test_new (void)
{
  BookupSection *section = bookup_section_new ();
  g_assert (BOOKUP_IS_SECTION (section));
  bookup_section_set_id (section, 10);
  g_assert (BOOKUP_IS_SECTION (section));
}

int
main (int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Section/new", test_new);
  return g_test_run ();
}
