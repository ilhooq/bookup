/* bookup-window.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-window.h"
#include "bookup-application.h"
#include "bookup-bookmarks.h"
#include "bookup-button-revealer.h"
#include "bookup-database.h"
#include "bookup-image.h"
#include "bookup-markdown-editor.h"
#include "bookup-page-info.h"
#include "bookup-page.h"
#include "bookup-pages-view.h"
#include "bookup-preferences-dialog.h"
#include "bookup-properties-dialog.h"
#include "bookup-section-dialog.h"
#include "bookup-section.h"
#include "bookup-sections-view.h"
#include "bookup-tags.h"
#include "bookup-theme-selector.h"
#include "bookup-trash.h"
#include "bookup-web-view.h"
#include "utils.h"
#include <glib/gi18n.h>
#include <string.h>

enum Mode
{
  VIEW_MODE,
  EDIT_MODE
};

#define GET_BOOKUP_WINDOW(widget) \
  (BOOKUP_WINDOW (gtk_widget_get_ancestor (widget, bookup_window_get_type ())))

struct _BookupWindow
{
  GtkApplicationWindow parent_instance;

  BookupApplication *app;
  BookupDatabase *db;

  /* Template widgets */
  GtkHeaderBar *header_bar;
  GtkButton *btn_display;
  GtkButton *btn_edit;
  GtkButton *btn_page_info;
  GtkToggleButton *btn_bookmark;
  GtkRevealer *info_bar;
  GtkPaned *main_pane;
  GtkPaned *page_pane;
  GtkStackSwitcher *btn_switch_mode;
  GtkStack *show_edit;
  GtkPopoverMenu *primary_menu;

  BookupMarkdownEditor *editor;
  BookupSectionsView *sections_view;
  BookupPagesView *pages_view;
  BookupBookmarks *bookmarks;
  BookupTags *tags;
  BookupTrash *trash;
  BookupPageInfo *page_info;
  BookupButtonRevealer *btn_search;

  enum Mode mode;

  BookupWebView *webview;

  BookupSection *loaded_section;
  BookupPage *loaded_page;

  GtkCssProvider *css_provider;
};

G_DEFINE_TYPE (BookupWindow, bookup_window, GTK_TYPE_APPLICATION_WINDOW)

static void save_loaded_page (BookupWindow *);

static void
update_css (BookupWindow *self, GSettings *settings)
{
  PangoFontDescription *font_desc;
  g_autoptr (GString) str = NULL;
  g_autofree char *font = NULL;

  g_assert (BOOKUP_IS_WINDOW (self));

  str = g_string_new ("textview.bookup-editor {\n");
  font = g_settings_get_string (settings, "editor-font");
  font_desc = pango_font_description_from_string (font);

  if (font_desc != NULL)
    {
      g_autofree char *css = font_description_to_css (font_desc);

      if (css != NULL)
        g_string_append_printf (str, "  %s\n", css);

      pango_font_description_free (font_desc);
    }

  g_string_append (str, "}");

  gtk_css_provider_load_from_string (self->css_provider, str->str);
}

static void
on_setting_changed (GSettings *settings, char *key, gpointer user_data)
{
  g_assert (G_IS_SETTINGS (settings));
  g_assert (BOOKUP_IS_WINDOW (user_data));

  BookupWindow *self = (BookupWindow *) user_data;

  if (g_str_equal (key, "editor-font"))
    update_css (self, settings);
}

static void
setup_editor (BookupWindow *self, GSettings *settings)
{
  g_autofree char *style_scheme_id = NULL;

  g_return_if_fail (BOOKUP_IS_WINDOW (self));

  style_scheme_id = g_settings_get_string (settings, "style-scheme");
  bookup_markdown_editor_set_style_scheme (self->editor, style_scheme_id);

  g_settings_bind (settings, "style-scheme",
                   self->editor, "style-scheme",
                   G_SETTINGS_BIND_GET);
}

static void
load_sections (BookupWindow *self)
{
  GListModel *model;
  model = bookup_database_fetch_rows (
      self->db,
      "SELECT * FROM section ORDER BY name ASC",
      BOOKUP_TYPE_SECTION);

  bookup_sections_view_set_model (self->sections_view, model);
}

static void
load_bookmarks (BookupWindow *self)
{
  GListModel *model;
  model = bookup_database_fetch_rows (
      self->db,
      "SELECT id, `name` FROM page WHERE starred=1 ORDER BY `name` ASC",
      BOOKUP_TYPE_PAGE);

  bookup_bookmarks_set_model (self->bookmarks, model);
}

static void
load_tags (BookupWindow *self)
{
  GListModel *model;
  model = bookup_database_fetch_rows (
      self->db,
      "SELECT * FROM tag ORDER BY name ASC",
      BOOKUP_TYPE_PAGE);

  bookup_tags_set_model (self->tags, model);
}

static void
load_trash (BookupWindow *self)
{
  GListModel *model;
  gboolean is_empty;

  model = bookup_database_fetch_rows (
      self->db,
      "SELECT id, `name` FROM page WHERE trash=1 ORDER BY `name` ASC",
      BOOKUP_TYPE_PAGE);

  bookup_trash_set_model (self->trash, model);

  is_empty = g_list_model_get_n_items (model) == 0;
  gtk_widget_set_visible (GTK_WIDGET (self->trash), !is_empty);
}

static void
pages_load_section (BookupWindow *self)
{
  g_autofree char *sql = NULL;
  GListModel *model;

  g_return_if_fail (BOOKUP_IS_SECTION (self->loaded_section));

  sql = g_strdup_printf (
      "SELECT id, name, section_id FROM page "
      "WHERE section_id = %d AND trash = 0 ORDER BY name ASC",
      bookup_section_get_id (self->loaded_section));

  model = bookup_database_fetch_rows (self->db, sql, BOOKUP_TYPE_PAGE);
  bookup_pages_view_load_section (self->pages_view, self->loaded_section, model);
  gtk_widget_set_sensitive (GTK_WIDGET (self->pages_view), TRUE);
}

static void
pages_load_search (BookupWindow *self, const char *search)
{
  g_autofree char *sql = NULL;
  g_autofree char *escaped_search = NULL;
  GListModel *model;

  escaped_search = bookup_database_escape_string (search);

  sql = g_strdup_printf (
      "SELECT id, name, section_id FROM page "
      "WHERE name LIKE '%%%s%%' AND trash = 0 ORDER BY name ASC",
      escaped_search);

  g_debug ("Search query: %s", sql);

  model = bookup_database_fetch_rows (self->db, sql, BOOKUP_TYPE_PAGE);
  bookup_pages_view_load_search (self->pages_view, search, model);
  gtk_widget_set_sensitive (GTK_WIDGET (self->pages_view), TRUE);
}

static void
pages_load_tag (BookupWindow *self, unsigned int tag_id, const char *tag)
{
  g_autofree char *sql = NULL;
  GListModel *model;

  sql = g_strdup_printf (
      "SELECT id, name, section_id FROM page "
      "INNER JOIN tag_page ON page.id = tag_page.page_id "
      "WHERE tag_page.tag_id = %u AND page.trash = 0 ORDER BY name ASC",
      tag_id);

  model = bookup_database_fetch_rows (self->db, sql, BOOKUP_TYPE_PAGE);
  bookup_pages_view_load_tag (self->pages_view, tag, model);
  gtk_widget_set_sensitive (GTK_WIDGET (self->pages_view), TRUE);
}

static void
page_info_load_tags (BookupWindow *self)
{
  g_autofree char *query = NULL;
  GListModel *model;

  g_return_if_fail (BOOKUP_IS_PAGE (self->loaded_page));

  query = g_strdup_printf (
      "SELECT id, `name` FROM tag "
      "INNER JOIN tag_page ON tag.id = tag_page.tag_id "
      "WHERE tag_page.page_id = %u ORDER BY `name` ASC",
      bookup_page_get_id (self->loaded_page));

  g_debug ("Page tags query: %s", query);

  model = bookup_database_fetch_rows (self->db, query, BOOKUP_TYPE_PAGE);
  bookup_page_info_set_model (self->page_info, model);

  model = bookup_database_fetch_rows (self->db, "SELECT * FROM tag", BOOKUP_TYPE_PAGE);
  bookup_page_info_set_entry_tag_completion_model (self->page_info, model);
}

static gboolean
section_is_empty (BookupWindow *self, BookupSection *section)
{
  g_autofree gchar *query = NULL;
  unsigned int id = bookup_section_get_id (section);

  query = g_strdup_printf ("SELECT * FROM page WHERE section_id = %d", id);

  if (bookup_database_count_rows (self->db, query))
    return FALSE;

  g_free (query);
  query = g_strdup_printf ("SELECT * FROM section WHERE parent_id = %d", id);

  if (bookup_database_count_rows (self->db, query))
    return FALSE;

  return TRUE;
}

static gboolean
close_request (GtkWindow *window)
{
  GSettings *settings;
  BookupWindow *self = BOOKUP_WINDOW (window);
  int is_maximized = gtk_window_is_maximized (window);
  settings = bookup_application_get_settings (self->app);
  g_settings_set_boolean (settings, "is-maximized", is_maximized);

  if (!is_maximized)
    {
      int height;
      int width;

      gtk_window_get_default_size (window, &width, &height);
      g_settings_set_int (settings, "width", width);
      g_settings_set_int (settings, "height", height);
    }

  if (BOOKUP_IS_PAGE (self->loaded_page))
    {
      save_loaded_page (self);
      g_settings_set_int (settings, "last-page", bookup_page_get_id (self->loaded_page));
    }

  g_settings_set_int (settings, "mode", self->mode);
  g_settings_set_int (settings, "main-pane-pos", gtk_paned_get_position (self->main_pane));
  g_settings_set_int (settings, "page-pane-pos", gtk_paned_get_position (self->page_pane));

  return GTK_WINDOW_CLASS (bookup_window_parent_class)->close_request (window);
}

static void
togglepane_changestate_cb (GSimpleAction *action,
                           GVariant *value,
                           gpointer user_data)
{
  BookupWindow *self;
  GSettings *settings;
  GtkWidget *nav1_pane;
  GtkWidget *nav2_pane;

  g_assert (BOOKUP_IS_WINDOW (user_data));
  self = BOOKUP_WINDOW (user_data);

  settings = bookup_application_get_settings (self->app);

  const char *type = g_variant_get_string (value, NULL);

  g_simple_action_set_state (action, value);

  nav1_pane = gtk_paned_get_start_child (self->main_pane);
  nav2_pane = gtk_paned_get_start_child (self->page_pane);

  if (g_str_equal (type, "two"))
    {
      gtk_widget_set_visible (nav1_pane, TRUE);
      gtk_widget_set_visible (nav2_pane, TRUE);
      g_settings_set_string (settings, "pane-state", "two");
    }
  else if (g_str_equal (type, "one"))
    {
      gtk_widget_set_visible (nav1_pane, FALSE);
      gtk_widget_set_visible (nav2_pane, TRUE);
      g_settings_set_string (settings, "pane-state", "one");
    }
  else
    {
      gtk_widget_set_visible (nav1_pane, FALSE);
      gtk_widget_set_visible (nav2_pane, FALSE);
      g_settings_set_string (settings, "pane-state", "none");
    }
}

static void
toggle_info (GtkToggleButton *btn, BookupWindow *self)
{
  g_assert (BOOKUP_IS_WINDOW (self));

  if (gtk_toggle_button_get_active (btn))
    {
      gtk_revealer_set_reveal_child (self->info_bar, TRUE);
      return;
    }

  gtk_revealer_set_reveal_child (self->info_bar, FALSE);
}

static void
on_tag_activate_cb (BookupWindow *self, BookupPage *tag)
{
  g_assert (BOOKUP_IS_WINDOW (self));
  g_assert (BOOKUP_IS_PAGE (tag));

  pages_load_tag (self, bookup_page_get_id (tag), bookup_page_get_name (tag));
}

static void
on_search_entry_mapped (GtkWidget *entry)
{
  /*
   * The search entry grabs the focus
   *  when the user click on the search button
   */
  gtk_widget_grab_focus (entry);
}

static void
search_changed_cb (GtkSearchEntry *entry, BookupWindow *self)
{
  const char *text;
  g_assert (BOOKUP_IS_WINDOW (self));
  text = gtk_editable_get_text (GTK_EDITABLE (entry));
  pages_load_search (self, text);
}

static void
stop_search_cb (GtkSearchEntry *entry, BookupWindow *self)
{
  g_assert (BOOKUP_IS_WINDOW (self));
  gtk_editable_set_text (GTK_EDITABLE (entry), "");
  pages_load_section (self);
}

static void
on_editor_first_line_changed (BookupWindow *self, gchar *first_line)
{
  g_assert (BOOKUP_IS_WINDOW (self));

  bookup_page_set_name (self->loaded_page, extract_title (first_line));
  bookup_pages_view_update_page (self->pages_view, self->loaded_page);

  g_debug ("New page title: %s", bookup_page_get_name (self->loaded_page));
}

static void
on_page_add_tag_cb (BookupWindow *self, char *tag)
{
  unsigned int page_id, tag_id;
  g_autofree char *query = NULL;
  g_autofree char *escaped_tag = NULL;
  g_autoptr (GObject) tag_record = NULL;

  g_assert (BOOKUP_IS_WINDOW (self));

  if (!BOOKUP_IS_PAGE (self->loaded_page))
    return;

  page_id = bookup_page_get_id (self->loaded_page);
  escaped_tag = bookup_database_escape_string (tag);
  query = g_strdup_printf ("SELECT * FROM tag WHERE name LIKE '%%%s%%'", escaped_tag);
  tag_record = bookup_database_fetch_row (self->db, query, BOOKUP_TYPE_PAGE);
  g_free (query);

  if (BOOKUP_IS_PAGE (tag_record))
    {
      tag_id = bookup_page_get_id (BOOKUP_PAGE (tag_record));
    }
  else
    {
      query = g_strdup_printf ("INSERT INTO tag (`name`) VALUES ('%s')", escaped_tag);
      bookup_database_exec (self->db, query);
      tag_id = bookup_database_get_last_insert_id (self->db);
      g_free (query);
      load_tags (self);
    }

  query = g_strdup_printf ("SELECT * FROM tag_page WHERE page_id=%u AND tag_id=%u",
                           page_id,
                           tag_id);
  if (bookup_database_count_rows (self->db, query))
    return;

  g_free (query);
  query = g_strdup_printf ("INSERT INTO tag_page (page_id, tag_id) VALUES (%u, %u)",
                           page_id,
                           tag_id);
  bookup_database_exec (self->db, query);
  page_info_load_tags (self);
}

static void
load_section (BookupWindow *self, unsigned int section_id)
{
  g_autofree char *sql = NULL;
  GObject *row;

  g_assert (BOOKUP_IS_WINDOW (self));

  if (self->loaded_section)
    {
      g_object_unref (self->loaded_section);
      self->loaded_section = NULL;
    }

  sql = g_strdup_printf ("SELECT * FROM section WHERE id = %d", section_id);
  row = bookup_database_fetch_row (self->db, sql, BOOKUP_TYPE_SECTION);

  if (BOOKUP_IS_SECTION (row))
    {
      self->loaded_section = BOOKUP_SECTION (row);
      pages_load_section (self);
    }
}

static void
save_loaded_page (BookupWindow *self)
{
  g_autofree char *html = NULL;
  g_autofree char *data = NULL;

  g_assert (BOOKUP_IS_WINDOW (self));

  if (bookup_markdown_editor_content_changed (self->editor))
    {
      data = bookup_markdown_editor_get_content (self->editor);
      bookup_page_set_data (self->loaded_page, data);

      html = markdown_to_html (data);

      const char *html_template = "<!doctype html>\n<meta charset='utf-8'>\n<head>"
                                  "<meta name='viewport' content='width=device-width, initial-scale=1'>\n"
                                  "</head>\n<body>\n%s</body>\n</html>";

      bookup_page_set_html_cache (self->loaded_page,
                                  g_strdup_printf (html_template, html));

      bookup_page_set_modification_date (self->loaded_page,
                                         g_date_time_to_unix (g_date_time_new_now_local ()));

      bookup_database_save (self->db, "Page", G_OBJECT (self->loaded_page));
    }
}

static gboolean
replace_image_cb (const GMatchInfo *info,
                  GString *result,
                  gpointer user_data)
{
  BookupWindow *self;
  GObject *row;
  g_autofree char *id = NULL;
  g_autofree char *query = NULL;

  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);
  id = g_match_info_fetch (info, 1);
  query = g_strdup_printf ("SELECT * FROM image WHERE id = %s", id);
  row = bookup_database_fetch_row (self->db, query, BOOKUP_TYPE_IMAGE);

  if (row)
    {
      const guint8 *data;
      gsize data_size;
      g_autofree char *mime_type = NULL;
      g_autofree char *b64_encoded = NULL;

      data = bookup_image_get_data (BOOKUP_IMAGE (row));
      data_size = bookup_image_get_data_size (BOOKUP_IMAGE (row));
      mime_type = g_content_type_guess (NULL, data, data_size, NULL);
      b64_encoded = g_base64_encode (data, data_size);

      g_debug ("Add image id %s, mime type: %s, data size : %lu",
               id, mime_type, data_size);

      g_string_append_printf (result, "<img src=\"data:%s;base64, %s\" alt=\"Image %s\">",
                              mime_type, b64_encoded, id);

      g_object_unref (row);
    }
  else
    {
      g_string_append_printf (result,
                              "<strong>Image %s not found in the database</strong>", id);
    }

  return FALSE;
}

static char *
prepare_html (BookupWindow *self, const char *html)
{
  g_autoptr (GRegex) regex_img = NULL;

  regex_img = g_regex_new ("<image (\\d+)>", G_REGEX_MULTILINE,
                           G_REGEX_MATCH_DEFAULT, NULL);

  return g_regex_replace_eval (regex_img,
                               html,
                               -1,
                               0,
                               G_REGEX_MATCH_DEFAULT,
                               replace_image_cb,
                               g_object_ref (self),
                               NULL);
}

static void
load_page (BookupWindow *self, unsigned int page_id)
{
  g_autofree char *query = NULL;
  const char *data;
  g_autofree char *html = NULL;
  GObject *row;

  g_assert (BOOKUP_IS_WINDOW (self));

  if (self->loaded_page)
    {
      save_loaded_page (self);
      g_object_unref (self->loaded_page);
      self->loaded_page = NULL;
    }

  query = g_strdup_printf ("SELECT * FROM page WHERE id = %d", page_id);

  row = bookup_database_fetch_row (self->db, query, BOOKUP_TYPE_PAGE);

  if (BOOKUP_IS_PAGE (row))
    {
      self->loaded_page = BOOKUP_PAGE (row);
      data = bookup_page_get_data (self->loaded_page);
      html = prepare_html (self, bookup_page_get_html_cache (self->loaded_page));

      gtk_window_set_title (GTK_WINDOW (self), bookup_page_get_name (self->loaded_page));
      bookup_markdown_editor_load_content (self->editor, data);

      bookup_web_view_load_html (self->webview, html);
      gtk_widget_set_sensitive (GTK_WIDGET (self->editor), TRUE);
      gtk_widget_set_sensitive (GTK_WIDGET (self->btn_switch_mode), TRUE);
      gtk_widget_set_sensitive (GTK_WIDGET (self->btn_bookmark), TRUE);
      gtk_widget_set_sensitive (GTK_WIDGET (self->btn_page_info), TRUE);

      if (bookup_page_get_starred (self->loaded_page))
        gtk_button_set_icon_name (GTK_BUTTON (self->btn_bookmark), "starred-symbolic");
      else
        gtk_button_set_icon_name (GTK_BUTTON (self->btn_bookmark), "non-starred-symbolic");

      gtk_toggle_button_set_active (self->btn_bookmark,
                                    bookup_page_get_starred (self->loaded_page));
      bookup_page_info_set_page (self->page_info, self->loaded_page);
      page_info_load_tags (self);
    }
}

static void
create_page_cb (GSimpleAction *action,
                GVariant *parameter,
                gpointer user_data)
{
  BookupWindow *self;
  g_autofree char *page_data = NULL;
  BookupPage *page;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);

  g_return_if_fail (self->loaded_section);

  page = bookup_page_new ();
  bookup_page_set_name (page, _("New Page"));
  page_data = g_strdup_printf ("# %s", _("New Page"));
  bookup_page_set_data (page, page_data);
  bookup_page_set_section_id (page, bookup_section_get_id (self->loaded_section));
  bookup_page_set_order_num (page, 0);
  bookup_page_set_creation_date (page, g_date_time_to_unix (g_date_time_new_now_local ()));
  bookup_page_set_starred (page, 0);
  bookup_database_save (self->db, "Page", G_OBJECT (page));

  bookup_pages_view_add_page (self->pages_view, page);

  gtk_stack_set_visible_child (self->show_edit, GTK_WIDGET (self->editor));
  self->mode = EDIT_MODE;

  bookup_pages_view_select_page (self->pages_view, page);
  load_page (self, bookup_page_get_id (page));
}

static void
delete_page_cb (GSimpleAction *action,
                GVariant *parameter,
                gpointer user_data)
{
  BookupWindow *self;
  BookupPage *page;
  unsigned int page_id;
  g_autofree char *query = NULL;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);
  page = bookup_pages_view_get_selected_page (self->pages_view);
  page_id = bookup_page_get_id (page);

  if (page_id > 0)
    {
      query = g_strdup_printf ("UPDATE page SET trash = 1 WHERE id = %u", page_id);
      bookup_database_exec (self->db, query);
      bookup_pages_view_delete_page (self->pages_view, page);
      load_trash (self);

      if (page_id == bookup_page_get_id (self->loaded_page))
        {
          bookup_page_info_remove_all (self->page_info);
          bookup_markdown_editor_empty (self->editor);
          bookup_web_view_load_html (self->webview, "");
          gtk_revealer_set_reveal_child (self->info_bar, FALSE);
          gtk_widget_set_sensitive (GTK_WIDGET (self->editor), FALSE);
          gtk_widget_set_sensitive (GTK_WIDGET (self->btn_bookmark), FALSE);
          gtk_widget_set_sensitive (GTK_WIDGET (self->btn_page_info), FALSE);

          g_object_unref (page);
          g_object_unref (self->loaded_page);
          self->loaded_page = NULL;
          gtk_window_set_title (GTK_WINDOW (self), "");
        }
    }
}

static void
edit_section_dialog_response (BookupSectionDialog *dialog,
                              int response_id,
                              gpointer user_data)
{
  BookupWindow *self;
  BookupSection *section;

  g_assert (BOOKUP_IS_WINDOW (user_data));

  if (response_id == GTK_RESPONSE_OK)
    {
      self = BOOKUP_WINDOW (user_data);
      section = bookup_section_dialog_get_section (dialog);

      if (!bookup_section_get_id (section))
        bookup_sections_view_add (self->sections_view, section);

      bookup_database_save (self->db, "section", G_OBJECT (section));

      if (self->loaded_section &&
          bookup_section_get_id (self->loaded_section) == bookup_section_get_id (section))
        {
          bookup_section_set_name (
              self->loaded_section,
              bookup_section_get_name (section));
        }
    }

  gtk_window_destroy (GTK_WINDOW (dialog));
}

static void
editsection_cb (GSimpleAction *action,
                GVariant *parameter,
                gpointer user_data)
{
  g_assert (G_IS_ACTION (action));
  g_assert (BOOKUP_IS_WINDOW (user_data));

  BookupWindow *self;
  BookupSectionDialog *dialog;
  const char *type = g_variant_get_string (parameter, NULL);
  BookupSection *section = NULL;

  self = BOOKUP_WINDOW (user_data);

  if (g_str_equal (type, "new") || g_str_equal (type, "new-child"))
    {
      section = bookup_section_new ();

      if (g_str_equal (type, "new-child"))
        {
          BookupSection *parent = bookup_sections_view_get_selected (self->sections_view);
          bookup_section_set_parent_id (section, bookup_section_get_id (parent));
        }
    }

  if (g_str_equal (type, "self"))
    section = bookup_sections_view_get_selected (self->sections_view);

  dialog = g_object_new (BOOKUP_TYPE_SECTION_DIALOG, NULL);
  bookup_section_dialog_set_section (dialog, section);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (self));
  gtk_window_present (GTK_WINDOW (dialog));
  g_signal_connect (dialog, "response", G_CALLBACK (edit_section_dialog_response), self);
}

static void
deletesection_cb (GSimpleAction *action,
                  GVariant *parameter,
                  gpointer user_data)
{
  BookupWindow *self;
  BookupSection *section;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);
  section = bookup_sections_view_get_selected (self->sections_view);

  if (!section_is_empty (self, section))
    {
      GtkAlertDialog *alert = gtk_alert_dialog_new (_("The section %s is not empty"),
                                                      bookup_section_get_name (section));
      gtk_alert_dialog_show (alert, GTK_WINDOW (self));
      return;
    }

  bookup_sections_view_remove_selected (self->sections_view);
  bookup_database_delete (self->db, "section", G_OBJECT (section));
}

static void
delete_tag_cb (GSimpleAction *action,
               GVariant *parameter,
               gpointer user_data)
{
  BookupWindow *self;
  int tag_id;
  g_autofree char *query = NULL;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);
  tag_id = bookup_tags_get_selected_tag_id (self->tags);

  if (tag_id > 0)
    {
      query = g_strdup_printf ("DELETE FROM tag WHERE id = %u", tag_id);
      bookup_database_exec (self->db, query);
      g_free (query);

      load_tags (self);

      query = g_strdup_printf ("DELETE FROM tag_page WHERE tag_id = %u", tag_id);
      bookup_database_exec (self->db, query);

      if (self->loaded_page)
        page_info_load_tags (self);
    }
}

static void
on_empty_trash_response (GObject *source_object,
                         GAsyncResult *res,
                         gpointer user_data)
{
  GtkAlertDialog *dialog = GTK_ALERT_DIALOG (source_object);
  BookupWindow *self = BOOKUP_WINDOW (user_data);
  g_autofree char *query = NULL;
  int button = gtk_alert_dialog_choose_finish (dialog, res, NULL);

  if (button == 1)
    {
      query = g_strdup ("DELETE FROM page WHERE trash = 1");
      bookup_database_exec (self->db, query);
      load_trash (self);
    }
}

static void
empty_trash_cb (GSimpleAction *action,
                GVariant *parameter,
                gpointer user_data)
{
  BookupWindow *self;
  GtkAlertDialog *dialog;
  const char *buttons[] = { _("_Cancel"), _("_Proceed"), NULL };

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);

  dialog = gtk_alert_dialog_new (_("Empty Trash"));
  gtk_alert_dialog_set_buttons (dialog, buttons);
  gtk_alert_dialog_set_cancel_button (dialog, 0);
  gtk_alert_dialog_set_default_button (dialog, 1);
  gtk_alert_dialog_set_detail (
      dialog,
      _("Are you sure you want to permanently delete all items from the trash?") );
  gtk_alert_dialog_choose (
      dialog,
      GTK_WINDOW (self),
      NULL,
      on_empty_trash_response,
      self);
}

static void
restore_page_cb (GSimpleAction *action,
                 GVariant *parameter,
                 gpointer user_data)
{
  BookupWindow *self;
  unsigned int page_id;
  g_autofree char *query = NULL;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);
  page_id = bookup_trash_get_selected_id (self->trash);

  if (page_id > 0)
    {
      query = g_strdup_printf ("UPDATE page SET trash = 0 WHERE id = %u", page_id);
      bookup_database_exec (self->db, query);
      load_trash (self);
      load_page (self, page_id);

      if (self->loaded_page)
        {
          load_section (self, bookup_page_get_section_id (self->loaded_page));
          bookup_sections_view_select_section (self->sections_view, self->loaded_section);
          bookup_pages_view_select_page (self->pages_view, self->loaded_page);
        }
    }
}

static void
delete_page_tag_cb (GSimpleAction *action,
                    GVariant *parameter,
                    gpointer user_data)
{
  unsigned int page_id, tag_id;
  g_autofree char *query = NULL;
  BookupWindow *self;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);

  if (self->loaded_page)
    {
      page_id = bookup_page_get_id (self->loaded_page);
      tag_id = bookup_page_info_get_selected_tag_id (self->page_info);
      query = g_strdup_printf ("DELETE FROM tag_page WHERE page_id = %u AND tag_id = %u",
                               page_id,
                               tag_id);
      bookup_database_exec (self->db, query);
      page_info_load_tags (self);
    }
}

static void
on_export_pdf (GObject *source,
               GAsyncResult *result,
               gpointer user_data)
{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GError) error = NULL;
  BookupWindow *self;

  g_assert (BOOKUP_IS_WINDOW (user_data));
  self = BOOKUP_WINDOW (user_data);

  file = gtk_file_dialog_save_finish (GTK_FILE_DIALOG (source), result, &error);

  if (!file)
    {
      g_warning ("%s", error->message);
      return;
    }

  bookup_web_view_print (self->webview, file);
}

static void
on_export_markdown (GObject *source,
                    GAsyncResult *result,
                    gpointer user_data)
{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GError) error = NULL;
  BookupWindow *self;
  const char *content;

  g_assert (BOOKUP_IS_WINDOW (user_data));
  self = BOOKUP_WINDOW (user_data);

  file = gtk_file_dialog_save_finish (GTK_FILE_DIALOG (source), result, &error);

  if (!file)
    {
      g_warning ("%s", error->message);
      return;
    }

  content = bookup_page_get_data (self->loaded_page);

  g_file_replace_contents (file,
                           content,
                           strlen (content),
                           NULL,
                           FALSE,
                           G_FILE_CREATE_REPLACE_DESTINATION,
                           NULL,
                           NULL,
                           &error);

  if (error)
    g_warning ("Failed to export markdown page: %s", error->message);
}

static void
export_cb (GSimpleAction *action,
           GVariant *parameter,
           gpointer user_data)
{
  g_autoptr (GtkFileDialog) chooser = NULL;
  g_autoptr (GtkFileFilter) filter = NULL;
  g_autoptr (GListStore) filters = NULL;
  g_autofree char *filename = NULL;
  BookupWindow *self;
  const char *type;
  const char *title;
  GAsyncReadyCallback callback;

  g_assert (G_IS_ACTION (action));
  g_assert (BOOKUP_IS_WINDOW (user_data));

  type = g_variant_get_string (parameter, NULL);
  self = BOOKUP_WINDOW (user_data);
  chooser = gtk_file_dialog_new ();
  filter = gtk_file_filter_new ();
  callback = NULL;
  title = bookup_page_get_name (self->loaded_page);

  if (g_str_equal (type, "pdf"))
    {
      filename = g_strdup_printf ("%s.pdf", title);
      gtk_file_dialog_set_title (chooser, _("Save as PDF"));
      gtk_file_dialog_set_initial_name (chooser, filename);
      gtk_file_filter_set_name (filter, _("PDF File"));
      gtk_file_filter_add_mime_type (filter, "application/pdf");
      gtk_file_filter_add_pattern (filter, "*.pdf");
      callback = on_export_pdf;
    }
  else if (g_str_equal (type, "markdown"))
    {
      filename = g_strdup_printf ("%s.md", title);
      gtk_file_dialog_set_title (chooser, _("Save as Mardown"));
      gtk_file_dialog_set_initial_name (chooser, filename);
      gtk_file_filter_set_name (filter, _("Markdown File"));
      gtk_file_filter_add_mime_type (filter, "text/markdown");
      gtk_file_filter_add_pattern (filter, "*.md");
      callback = on_export_markdown;
    }

  filters = g_list_store_new (GTK_TYPE_FILE_FILTER);
  g_list_store_append (filters, filter);
  gtk_file_dialog_set_filters (chooser, G_LIST_MODEL (filters));

  gtk_file_dialog_save (
      chooser,
      GTK_WINDOW (self),
      NULL,
      callback,
      g_object_ref (user_data));
}

static void
app_on_book_loaded (BookupApplication *app, gpointer user_data)
{
  BookupWindow *self;
  GSettings *settings;
  GAction *action;
  int last_page_id;

  g_assert (BOOKUP_IS_APPLICATION (app));
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);
  settings = bookup_application_get_settings (app);
  last_page_id = g_settings_get_int (settings, "last-page");
  gtk_widget_set_sensitive (GTK_WIDGET (self->main_pane), TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (self->btn_search), TRUE);

  /* The database should be now ready to use */
  self->db = bookup_application_get_database (app);

  load_sections (self);
  load_bookmarks (self);
  load_tags (self);
  load_trash (self);

  if (last_page_id)
    {
      load_page (self, last_page_id);

      if (self->loaded_page)
        {
          load_section (self, bookup_page_get_section_id (self->loaded_page));
          bookup_sections_view_select_section (self->sections_view, self->loaded_section);
          bookup_pages_view_select_page (self->pages_view, self->loaded_page);
        }
    }

  if ((action = g_action_map_lookup_action (G_ACTION_MAP (self), "book-properties")))
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), TRUE);
}

static void
app_on_book_closed (BookupApplication *app, gpointer user_data)
{
  BookupWindow *self;
  GAction *action;

  g_assert (BOOKUP_IS_APPLICATION (app));
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);

  if (self->loaded_page)
    {
      save_loaded_page (self);
      g_object_unref (self->loaded_page);
      self->loaded_page = NULL;
    }

  bookup_sections_view_remove_all (self->sections_view);
  bookup_pages_view_remove_all (self->pages_view);
  bookup_bookmarks_remove_all (self->bookmarks);
  bookup_tags_remove_all (self->tags);
  bookup_page_info_remove_all (self->page_info);
  bookup_trash_set_model (self->trash, NULL);
  gtk_widget_set_visible (GTK_WIDGET (self->trash), 0);

  bookup_markdown_editor_empty (self->editor);
  bookup_web_view_load_html (self->webview, "");

  gtk_revealer_set_reveal_child (self->info_bar, FALSE);

  gtk_widget_set_sensitive (GTK_WIDGET (self->editor), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (self->btn_switch_mode), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (self->main_pane), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (self->btn_search), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (self->btn_bookmark), FALSE);
  gtk_widget_set_sensitive (GTK_WIDGET (self->btn_page_info), FALSE);

  if ((action = g_action_map_lookup_action (G_ACTION_MAP (self), "book-properties")))
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), FALSE);
}

static void
on_dialog_choose_book (GObject *source,
                       GAsyncResult *result,
                       gpointer user_data)
{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree char *filepath = NULL;
  BookupWindow *self;

  g_assert (BOOKUP_IS_WINDOW (user_data));
  self = BOOKUP_WINDOW (user_data);

  file = gtk_file_dialog_open_finish (GTK_FILE_DIALOG (source), result, &error);

  if (!file)
    {
      g_warning ("%s", error->message);
      return;
    }

  filepath = g_file_get_path (file);
  bookup_application_open_book (self->app, filepath, FALSE);
}

static void
on_dialog_create_book (GObject *source,
                       GAsyncResult *result,
                       gpointer user_data)
{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree char *filepath = NULL;
  BookupWindow *self;

  g_assert (BOOKUP_IS_WINDOW (user_data));
  self = BOOKUP_WINDOW (user_data);

  file = gtk_file_dialog_save_finish (GTK_FILE_DIALOG (source), result, &error);

  if (!file)
    {
      g_warning ("%s", error->message);
      return;
    }

  filepath = g_file_get_path (file);
  bookup_application_open_book (self->app, filepath, TRUE);
}

static void
open_book_cb (GSimpleAction *action,
              GVariant *parameter,
              gpointer user_data)
{
  g_autoptr (GtkFileDialog) chooser = NULL;
  g_autoptr (GtkFileFilter) filter = NULL;
  g_autoptr (GListStore) filters = NULL;
  GtkWindow *window;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (GTK_IS_WINDOW (user_data));

  window = GTK_WINDOW (user_data);
  chooser = gtk_file_dialog_new ();
  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, _("Bookup database"));
  gtk_file_filter_add_suffix (filter, "bdb");
  filters = g_list_store_new (GTK_TYPE_FILE_FILTER);
  g_list_store_append (filters, filter);
  gtk_file_dialog_set_filters (chooser, G_LIST_MODEL (filters));

  gtk_file_dialog_open (
      chooser,
      window,
      NULL,
      on_dialog_choose_book,
      g_object_ref (user_data));
}

static void
new_book_cb (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
  g_autoptr (GtkFileDialog) chooser = NULL;
  GtkWindow *window;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (GTK_IS_WINDOW (user_data));

  window = GTK_WINDOW (user_data);
  chooser = gtk_file_dialog_new ();
  gtk_file_dialog_set_title (chooser, _("Create new book"));
  gtk_file_dialog_set_initial_name (chooser, _("New book.bdb"));

  gtk_file_dialog_save (
      chooser,
      window,
      NULL,
      on_dialog_create_book,
      g_object_ref (user_data));
}

static void
open_preferences_dialog_cb (GSimpleAction *action,
                            GVariant *parameter,
                            gpointer user_data)
{
  AdwDialog *dialog = NULL;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (GTK_IS_WINDOW (user_data));

  dialog = ADW_DIALOG (g_object_new (BOOKUP_TYPE_PREFERENCES_DIALOG, NULL));
  adw_dialog_present (dialog, GTK_WIDGET (user_data));
}

static void
open_properties_dialog_cb (GSimpleAction *action,
                           GVariant *parameter,
                           gpointer user_data)
{
  GtkWidget *dialog;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (GTK_IS_WINDOW (user_data));

  dialog = bookup_properties_dialog_new (user_data);
  gtk_window_present (GTK_WINDOW (dialog));
}

static void
initiate_search (GSimpleAction *action,
                 GVariant *parameter,
                 gpointer user_data)
{
  BookupWindow *self;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);
  bookup_button_revealer_reveal (self->btn_search, TRUE);
}

static void
toggle_show_edit (GSimpleAction *action,
                  GVariant *parameter,
                  gpointer user_data)
{
  BookupWindow *self;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);

  if (self->mode == VIEW_MODE)
    {
      gtk_stack_set_visible_child (self->show_edit, GTK_WIDGET (self->editor));
      self->mode = EDIT_MODE;
    }
  else
    {
      gtk_stack_set_visible_child (self->show_edit, GTK_WIDGET (self->webview));
      self->mode = VIEW_MODE;
    }
}

static void
toggle_sidebar (GSimpleAction *action,
                GVariant *parameter,
                gpointer user_data)
{
  BookupWindow *self;
  GSettings *settings;
  GtkWidget *nav1_pane;
  GtkWidget *nav2_pane;

  GAction *togglepane_action;
  const char *state;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);

  togglepane_action = g_action_map_lookup_action (G_ACTION_MAP (self), "togglepane");
  settings = bookup_application_get_settings (self->app);
  nav1_pane = gtk_paned_get_start_child (self->main_pane);
  nav2_pane = gtk_paned_get_start_child (self->page_pane);

  if (gtk_widget_get_visible (nav1_pane) || gtk_widget_get_visible (nav2_pane))
    {
      gtk_widget_set_visible (nav1_pane, FALSE);
      gtk_widget_set_visible (nav2_pane, FALSE);
      state = "none";
    }
  else
    {
      gtk_widget_set_visible (nav1_pane, TRUE);
      gtk_widget_set_visible (nav2_pane, TRUE);
      state = "two";
    }

  g_settings_set_string (settings, "pane-state", state);
  g_action_change_state (togglepane_action, g_variant_new_string (state));
}

static void
toggle_page_info (GSimpleAction *action,
                  GVariant *parameter,
                  gpointer user_data)
{
  BookupWindow *self;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);

  gtk_revealer_set_reveal_child (self->info_bar,
                                 !gtk_revealer_get_reveal_child (self->info_bar));
}

static void
toggle_bookmark (GSimpleAction *action,
                 GVariant *parameter,
                 gpointer user_data)
{
  BookupWindow *self;
  g_autofree char *query = NULL;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_WINDOW (user_data));

  self = BOOKUP_WINDOW (user_data);

  if (!self->loaded_page)
    return;

  if (!bookup_page_get_starred (self->loaded_page))
    {
      gtk_button_set_icon_name (GTK_BUTTON (self->btn_bookmark), "starred-symbolic");
      bookup_page_set_starred (self->loaded_page, TRUE);
    }
  else
    {
      gtk_button_set_icon_name (GTK_BUTTON (self->btn_bookmark), "non-starred-symbolic");
      bookup_page_set_starred (self->loaded_page, FALSE);
    }
  gtk_toggle_button_set_active (self->btn_bookmark,
                                bookup_page_get_starred (self->loaded_page));

  query = g_strdup_printf ("UPDATE page SET starred=%u WHERE id=%u",
                           bookup_page_get_starred (self->loaded_page),
                           bookup_page_get_id (self->loaded_page));

  bookup_database_exec (self->db, query);
  load_bookmarks (self);
}

static void
on_dialog_choose_image (GObject *source,
                        GAsyncResult *result,
                        gpointer user_data)
{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree guint8 *contents = NULL;
  gsize contents_size = 0;

  BookupWindow *self;

  g_assert (BOOKUP_IS_WINDOW (user_data));
  self = BOOKUP_WINDOW (user_data);

  file = gtk_file_dialog_open_finish (GTK_FILE_DIALOG (source), result, &error);

  if (!file)
    {
      g_warning ("%s", error->message);
      return;
    }

  if (!g_file_load_contents (file, NULL, (gchar **) &contents,
                             &contents_size, NULL, &error))
    {
      g_warning ("Error loading file contents: %s", error->message);
      return;
    }

  if (bookup_database_exec_with_blob (self->db, "INSERT INTO image (data) VALUES (?)",
                                      contents, contents_size))
    {
      bookup_markdown_editor_insert_image (
          self->editor,
          bookup_database_get_last_insert_id (self->db));
    }
}

static void
insert_image_cb (GSimpleAction *action,
                 GVariant *parameter,
                 gpointer user_data)
{
  g_autoptr (GtkFileDialog) chooser = NULL;
  g_autoptr (GtkFileFilter) filter = NULL;
  g_autoptr (GListStore) filters = NULL;
  GtkWindow *window;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (GTK_IS_WINDOW (user_data));

  window = GTK_WINDOW (user_data);
  chooser = gtk_file_dialog_new ();
  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, _("Choose an image"));
  gtk_file_filter_add_mime_type (filter, "image/jpeg");
  gtk_file_filter_add_mime_type (filter, "image/png");
  gtk_file_filter_add_mime_type (filter, "image/gif");
  gtk_file_filter_add_mime_type (filter, "image/svg+xml");
  filters = g_list_store_new (GTK_TYPE_FILE_FILTER);
  g_list_store_append (filters, filter);
  gtk_file_dialog_set_filters (chooser, G_LIST_MODEL (filters));

  gtk_file_dialog_open (
      chooser,
      window,
      NULL,
      on_dialog_choose_image,
      g_object_ref (user_data));
}

static void
on_show_editor_cb (BookupWindow *self)
{
  g_assert (BOOKUP_IS_WINDOW (self));

  self->mode = EDIT_MODE;
}

static void
on_show_webview_cb (BookupWindow *self)
{
  g_assert (BOOKUP_IS_WINDOW (self));

  self->mode = VIEW_MODE;

  if (BOOKUP_IS_PAGE (self->loaded_page))
    load_page (self, bookup_page_get_id (self->loaded_page));
}

static void
on_page_activate_cb (BookupWindow *self, BookupPage *page)
{
  g_assert (BOOKUP_IS_WINDOW (self));
  g_assert (BOOKUP_IS_PAGE (page));

  load_page (self, bookup_page_get_id (page));
}

static void
on_section_activate_cb (BookupWindow *self, BookupSection *section)
{
  g_assert (BOOKUP_IS_WINDOW (self));
  g_assert (BOOKUP_IS_SECTION (section));

  load_section (self, bookup_section_get_id (section));
}

static void
on_sections_view_drop_section_cb (BookupWindow *self,
                                  int section_id,
                                  int new_parent_section_id)
{
  g_assert (BOOKUP_IS_WINDOW (self));
  g_autofree char *query = NULL;

  query = g_strdup_printf ("UPDATE section SET parent_id = %i WHERE id = %i",
                           new_parent_section_id, section_id);
  bookup_database_exec (self->db, query);
}

static void
on_sections_view_drop_page_cb (BookupWindow *self, int page_id, int new_section_id)
{
  g_autoptr (GObject) page = NULL;
  g_autofree char *query = NULL;

  g_assert (BOOKUP_IS_WINDOW (self));

  query = g_strdup_printf ("SELECT id, name FROM page WHERE id = %d", page_id);
  page = bookup_database_fetch_row (self->db, query, BOOKUP_TYPE_PAGE);

  if (BOOKUP_IS_PAGE (page))
    {
      bookup_pages_view_delete_page (self->pages_view, BOOKUP_PAGE (page));
      g_free (query);
      query = g_strdup_printf ("UPDATE page SET section_id = %u WHERE id = %u",
                               new_section_id, page_id);
      bookup_database_exec (self->db, query);
    }
}

static void
load_geometry (BookupWindow *self, GSettings *settings)
{
  int width = g_settings_get_int (settings, "width");
  int height = g_settings_get_int (settings, "height");
  gtk_window_set_default_size (GTK_WINDOW (self), width, height);
  gtk_paned_set_position (self->main_pane, g_settings_get_int (settings, "main-pane-pos"));
  gtk_paned_set_position (self->page_pane, g_settings_get_int (settings, "page-pane-pos"));

  if (g_settings_get_boolean (settings, "is-maximized"))
    gtk_window_maximize (GTK_WINDOW (self));
}

static void
on_notify_application (BookupWindow *self, GParamSpec *pspec)
{
  BookupApplication *app;
  GSettings *settings;
  g_autoptr (GSimpleActionGroup) group = NULL;
  g_autoptr (GAction) settings_action = NULL;
  g_autoptr (GtkAlertDialog) alert = NULL;

  /* We only lookup the application property */
  g_assert (g_str_equal (g_param_spec_get_name (pspec), "application"));
  g_assert (BOOKUP_IS_WINDOW (self));

  app = BOOKUP_APPLICATION (gtk_window_get_application (GTK_WINDOW (self)));
  g_assert (BOOKUP_IS_APPLICATION (app));

  self->app = BOOKUP_APPLICATION (app);
  settings = bookup_application_get_settings (app);
  self->mode = g_settings_get_int (settings, "mode");

  g_signal_connect (app, "book-loaded", G_CALLBACK (app_on_book_loaded), self);
  g_signal_connect (app, "book-closed", G_CALLBACK (app_on_book_closed), self);
  g_signal_connect (settings, "changed", G_CALLBACK (on_setting_changed), self);

  char *database_filename = g_settings_get_string (settings, "bookup-database");

  if (strlen (database_filename) && g_file_test (database_filename, G_FILE_TEST_EXISTS))
    bookup_application_open_book (app, database_filename, FALSE);
  else
    {
      alert = gtk_alert_dialog_new (_("The book file '%s' has been renamed or deleted."),
                                      database_filename);
      gtk_alert_dialog_show (alert, GTK_WINDOW (self));
    }

  if (self->mode == VIEW_MODE)
    gtk_stack_set_visible_child (self->show_edit, GTK_WIDGET (self->webview));

  update_css (self, settings);
  setup_editor (self, settings);
  bookup_web_view_set_custom_css (self->webview, g_settings_get_string (settings, "custom-css"));
  g_settings_bind (settings, "custom-css",
                   self->webview, "custom-css",
                   G_SETTINGS_BIND_GET);
  bookup_web_view_set_custom_script (self->webview, g_settings_get_string (settings, "custom-js"));
  g_settings_bind (settings, "custom-js",
                   self->webview, "custom-script",
                   G_SETTINGS_BIND_GET);

  GAction *action = g_action_map_lookup_action (G_ACTION_MAP (self), "togglepane");
  g_action_change_state (
      action,
      g_variant_new_string (g_settings_get_string (settings, "pane-state")));

  group = g_simple_action_group_new ();
  settings_action = g_settings_create_action (settings, "style-variant");
  g_action_map_add_action (G_ACTION_MAP (group), settings_action);
  gtk_widget_insert_action_group (GTK_WIDGET (self),
                                  "settings",
                                  G_ACTION_GROUP (group));

  load_geometry (self, settings);
}

static void
bookup_window_class_init (BookupWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GtkWindowClass *window_class = GTK_WINDOW_CLASS (klass);
  window_class->close_request = close_request;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/gitlab/ilhooq/Bookup/bookup-window.ui");
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, btn_switch_mode);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, show_edit);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, editor);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, webview);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, main_pane);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, page_pane);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, page_info);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, sections_view);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, pages_view);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, bookmarks);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, trash);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, btn_bookmark);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, btn_search);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, btn_page_info);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, tags);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, info_bar);
  gtk_widget_class_bind_template_child (widget_class, BookupWindow, primary_menu);

  gtk_widget_class_bind_template_callback (widget_class, on_section_activate_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_sections_view_drop_section_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_sections_view_drop_page_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_page_activate_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_show_editor_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_show_webview_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_editor_first_line_changed);
  gtk_widget_class_bind_template_callback (widget_class, search_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, stop_search_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_tag_activate_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_page_add_tag_cb);
  gtk_widget_class_bind_template_callback (widget_class, toggle_info);
  gtk_widget_class_bind_template_callback (widget_class, on_search_entry_mapped);
}

static void
bookup_window_init (BookupWindow *self)
{
  static const GActionEntry win_entries[] = {
    { .name = "new-book", .activate = new_book_cb },
    { .name = "open-book", .activate = open_book_cb },
    { .name = "show-preferences", .activate = open_preferences_dialog_cb },
    { .name = "book-properties", .activate = open_properties_dialog_cb },
    { .name = "editsection", .activate = editsection_cb, .parameter_type = "s" },
    { .name = "deletesection", .activate = deletesection_cb },
    { .name = "delete_tag", .activate = delete_tag_cb },
    { .name = "empty-trash", .activate = empty_trash_cb },
    { .name = "restore-page", .activate = restore_page_cb },
    { .name = "delete_page_tag", .activate = delete_page_tag_cb },
    { .name = "create-page", .activate = create_page_cb },
    { .name = "delete-page", .activate = delete_page_cb },
    { .name = "togglepane", .parameter_type = "s", .state = "'none'", .change_state = togglepane_changestate_cb },
    { .name = "initiate-search", .activate = initiate_search },
    { .name = "toggle-show-edit", .activate = toggle_show_edit },
    { .name = "toggle-sidebar", .activate = toggle_sidebar },
    { .name = "toggle-page-info", .activate = toggle_page_info },
    { .name = "toggle-bookmark", .activate = toggle_bookmark },
    { .name = "insert-image", .activate = insert_image_cb },
    { .name = "export", .activate = export_cb, .parameter_type = "s" },
  };

  GAction *action;

  self->db = NULL;
  self->loaded_page = NULL;
  self->loaded_section = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->css_provider = gtk_css_provider_new ();
  gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                              GTK_STYLE_PROVIDER (self->css_provider),
                                              GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  g_signal_connect (self,
                    "notify::application",
                    G_CALLBACK (on_notify_application),
                    NULL);

  gtk_popover_menu_add_child (self->primary_menu,
                              bookup_theme_selector_new (),
                              "theme");

  /* The action group name for a GtkApplicationWindow is "win" */
  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   win_entries, G_N_ELEMENTS (win_entries),
                                   self);
  if ((action = g_action_map_lookup_action (G_ACTION_MAP (self), "book-properties")))
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), FALSE);
}
