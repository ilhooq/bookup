/* bookup-section-dialog.h
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "bookup-section.h"
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BOOKUP_TYPE_SECTION_DIALOG (bookup_section_dialog_get_type ())

G_DECLARE_FINAL_TYPE (BookupSectionDialog, bookup_section_dialog, BOOKUP, SECTION_DIALOG, GtkWindow)

void
bookup_section_dialog_set_section (BookupSectionDialog *dialog,
                                   BookupSection *section);

BookupSection *
bookup_section_dialog_get_section (BookupSectionDialog *dialog);

G_END_DECLS

