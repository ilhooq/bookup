/* bookup-database.h
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define BOOKUP_TYPE_DATABASE (bookup_database_get_type ())

G_DECLARE_FINAL_TYPE (BookupDatabase, bookup_database, BOOKUP, DATABASE, GObject)

BookupDatabase *bookup_database_new (const char *filename,
                                     const char *schema_path);

gboolean bookup_database_exec (BookupDatabase *db, const char *query);

gboolean bookup_database_exec_with_blob (BookupDatabase *self,
                                         const char *query,
                                         const guint8 *blob_data,
                                         gsize blob_size);

GListModel *bookup_database_fetch_rows (BookupDatabase *db,
                                        const char *sql,
                                        GType type);

GObject *bookup_database_fetch_row (BookupDatabase *db,
                                    const char *sql,
                                    GType type);

gboolean bookup_database_save (BookupDatabase *db,
                               char *table,
                               GObject *object);

gboolean bookup_database_delete (BookupDatabase *db,
                                 char *table,
                                 GObject *object);

char *bookup_database_escape_string (const char *string);

int bookup_database_get_last_insert_id (BookupDatabase *db);

int bookup_database_count_rows (BookupDatabase *db, const char *query);

const char *bookup_database_get_filename (BookupDatabase *db);

G_END_DECLS
