/* bookup-page.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include "bookup-page.h"
#include <stdlib.h>

struct _BookupPage
{
  GObject parent_instance;
  unsigned int id;
  char *name;
  char *data;
  char *html_cache;
  unsigned int creation_date;
  unsigned int modification_date;
  unsigned int section_id;
  int order_num;
  gboolean starred;
};

G_DEFINE_TYPE (BookupPage, bookup_page, G_TYPE_OBJECT)

enum
{
  PROP_ID = 1,
  PROP_NAME,
  PROP_DATA,
  PROP_HTML_CACHE,
  PROP_CREATION_DATE,
  PROP_MODIFICATION_DATE,
  PROP_SECTION_ID,
  PROP_ORDER_NUM,
  PROP_STARRED,
  N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

BookupPage *
bookup_page_new ()
{
  BookupPage *self = g_object_new (BOOKUP_TYPE_PAGE, NULL);

  return self;
}

static void
bookup_page_finalize (GObject *object)
{
  BookupPage *page = BOOKUP_PAGE (object);
  g_free (page->name);
  g_free (page->data);
  g_free (page->html_cache);
  G_OBJECT_CLASS (bookup_page_parent_class)->finalize (object);
}

static void
bookup_page_set_property (GObject *object,
                          unsigned int property_id,
                          const GValue *value,
                          GParamSpec *pspec)
{
  BookupPage *self = BOOKUP_PAGE (object);

  switch (property_id)
    {
    case PROP_ID:
      self->id = g_value_get_uint (value);
      break;
    case PROP_NAME:
      g_free (self->name);
      self->name = g_value_dup_string (value);
      break;
    case PROP_DATA:
      g_free (self->data);
      self->data = g_value_dup_string (value);
      break;
    case PROP_HTML_CACHE:
      g_free (self->html_cache);
      self->html_cache = g_value_dup_string (value);
      break;
    case PROP_CREATION_DATE:
      self->creation_date = g_value_get_uint (value);
      break;
    case PROP_MODIFICATION_DATE:
      self->modification_date = g_value_get_uint (value);
      break;
    case PROP_SECTION_ID:
      self->section_id = g_value_get_uint (value);
      break;
    case PROP_ORDER_NUM:
      self->order_num = g_value_get_int (value);
      break;
    case PROP_STARRED:
      self->starred = g_value_get_boolean (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
bookup_page_get_property (GObject *object, unsigned int property_id, GValue *value, GParamSpec *pspec)
{
  BookupPage *self = BOOKUP_PAGE (object);

  switch (property_id)
    {
    case PROP_ID:
      g_value_set_uint (value, self->id);
      break;
    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;
    case PROP_DATA:
      g_value_set_string (value, self->data);
      break;
    case PROP_HTML_CACHE:
      g_value_set_string (value, self->html_cache);
      break;
    case PROP_CREATION_DATE:
      g_value_set_uint (value, self->creation_date);
      break;
    case PROP_MODIFICATION_DATE:
      g_value_set_uint (value, self->modification_date);
      break;
    case PROP_SECTION_ID:
      g_value_set_uint (value, self->section_id);
      break;
    case PROP_ORDER_NUM:
      g_value_set_int (value, self->order_num);
      break;
    case PROP_STARRED:
      g_value_set_boolean (value, self->starred);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
bookup_page_class_init (BookupPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->set_property = bookup_page_set_property;
  object_class->get_property = bookup_page_get_property;
  object_class->finalize = bookup_page_finalize;

  properties[PROP_ID] =
      g_param_spec_uint ("id",
                         "ID",
                         "The page ID",
                         0,
                         G_MAXINT,
                         0,
                         G_PARAM_READWRITE);

  properties[PROP_NAME] =
      g_param_spec_string ("name",
                           "Name",
                           "The name of the page",
                           NULL,
                           G_PARAM_READWRITE);

  properties[PROP_DATA] =
      g_param_spec_string ("data",
                           "Data",
                           "The markdown data of the page",
                           NULL,
                           G_PARAM_READWRITE);

  properties[PROP_HTML_CACHE] =
      g_param_spec_string ("html-cache",
                           "HTML cache",
                           "The html of the page",
                           NULL,
                           G_PARAM_READWRITE);

  properties[PROP_CREATION_DATE] =
      g_param_spec_uint ("creation-date",
                         "Creation date",
                         "The page creation date",
                         0,
                         G_MAXINT,
                         0,
                         G_PARAM_READWRITE);

  properties[PROP_MODIFICATION_DATE] =
      g_param_spec_uint ("modification-date",
                         "Modification date",
                         "The page modification date",
                         0,
                         G_MAXINT,
                         0,
                         G_PARAM_READWRITE);

  properties[PROP_SECTION_ID] =
      g_param_spec_uint ("section-id",
                         "Section ID",
                         "The page section ID",
                         0,
                         G_MAXINT,
                         0,
                         G_PARAM_READWRITE);

  properties[PROP_ORDER_NUM] =
      g_param_spec_int ("order-num",
                        "Order",
                        "The order the page",
                        G_MININT,
                        G_MAXINT,
                        0,
                        G_PARAM_READWRITE);
  properties[PROP_STARRED] =
      g_param_spec_boolean ("starred",
                            "Starred",
                            "A flag that indicates if the page is starred",
                            FALSE,
                            G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

static void
bookup_page_init (BookupPage *page)
{
  page->name = NULL;
  page->data = NULL;
  page->html_cache = NULL;
}

unsigned int
bookup_page_get_id (BookupPage *page)
{
  g_return_val_if_fail (page != NULL, 0);
  return page->id;
}

void
bookup_page_set_id (BookupPage *page, unsigned int id)
{
  g_assert (page != NULL);
  page->id = id;
  g_object_notify_by_pspec (G_OBJECT (page), properties[PROP_ID]);
}

char *
bookup_page_get_name (BookupPage *page)
{
  g_return_val_if_fail (page != NULL, NULL);
  return page->name;
}

void
bookup_page_set_name (BookupPage *page, char *name)
{
  g_return_if_fail (page != NULL);
  if (page->name != NULL)
    g_free (page->name);
  page->name = g_strdup (name);
  g_object_notify_by_pspec (G_OBJECT (page), properties[PROP_NAME]);
}

char *
bookup_page_get_data (BookupPage *page)
{
  g_return_val_if_fail (page != NULL, NULL);
  return page->data;
}

void
bookup_page_set_data (BookupPage *page, char *data)
{
  g_return_if_fail (page != NULL);
  if (page->data != NULL)
    g_free (page->data);
  page->data = g_strdup (data);
}

char *
bookup_page_get_html_cache (BookupPage *page)
{
  g_return_val_if_fail (page != NULL, NULL);
  return page->html_cache;
}

void
bookup_page_set_html_cache (BookupPage *page, char *html_cache)
{
  g_return_if_fail (page != NULL);
  if (page->html_cache != NULL)
    g_free (page->html_cache);
  page->html_cache = g_strdup (html_cache);
}

unsigned int
bookup_page_get_creation_date (BookupPage *page)
{
  g_return_val_if_fail (page != NULL, 0);
  return page->creation_date;
}

void
bookup_page_set_creation_date (BookupPage *page, unsigned int creation_date)
{
  g_return_if_fail (page != NULL);
  page->creation_date = creation_date;
}

unsigned int
bookup_page_get_modification_date (BookupPage *page)
{
  g_return_val_if_fail (page != NULL, 0);
  return page->modification_date;
}

void
bookup_page_set_modification_date (BookupPage *page, unsigned int modification_date)
{
  g_return_if_fail (page != NULL);
  page->modification_date = modification_date;
}

unsigned int
bookup_page_get_section_id (BookupPage *page)
{
  g_return_val_if_fail (page != NULL, 0);
  return page->section_id;
}

void
bookup_page_set_section_id (BookupPage *page, unsigned int section_id)
{
  g_return_if_fail (page != NULL);
  page->section_id = section_id;
  g_object_notify_by_pspec (G_OBJECT (page), properties[PROP_SECTION_ID]);
}

unsigned int
bookup_page_get_order_num (BookupPage *page)
{
  g_return_val_if_fail (page != NULL, 0);
  return page->order_num;
}

void
bookup_page_set_order_num (BookupPage *page, unsigned int order_num)
{
  g_assert (page != NULL);
  page->order_num = order_num;
}

gboolean
bookup_page_get_starred (BookupPage *page)
{
  g_return_val_if_fail (page != NULL, 0);
  return page->starred;
}

void
bookup_page_set_starred (BookupPage *page, gboolean starred)
{
  g_return_if_fail (page != NULL);
  page->starred = starred;
  g_object_notify_by_pspec (G_OBJECT (page), properties[PROP_STARRED]);
}
