/* bookup-pages-view.h
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "bookup-page.h"
#include "bookup-section.h"
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BOOKUP_TYPE_PAGES_VIEW (bookup_pages_view_get_type ())

G_DECLARE_FINAL_TYPE (BookupPagesView, bookup_pages_view, BOOKUP, PAGES_VIEW, GtkBox)

void
bookup_pages_view_load_section (BookupPagesView *self, BookupSection *section, GListModel *model);

void
bookup_pages_view_select_page (BookupPagesView *self, BookupPage *page);

void
bookup_pages_view_add_page (BookupPagesView *self, BookupPage *page);

void
bookup_pages_view_update_page (BookupPagesView *self, BookupPage *page);

void
bookup_pages_view_delete_page (BookupPagesView *self, BookupPage *page);

void
bookup_pages_view_remove_all (BookupPagesView *self);

BookupPage *
bookup_pages_view_get_selected_page (BookupPagesView *self);

void
bookup_pages_view_load_search (BookupPagesView *self, const char *search, GListModel *model);

void
bookup_pages_view_load_tag (BookupPagesView *self, const char *tag, GListModel *model);

G_END_DECLS
