/* bookup-web-view.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-web-view.h"
#include "config.h"
#include <glib/gi18n.h>
#include <unix-print/gtk/gtkunixprint.h>

struct _BookupWebView
{
  WebKitWebView parent_instance;

  WebKitUserStyleSheet *custom_stylesheet;
  WebKitUserScript *custom_script;
};

G_DEFINE_FINAL_TYPE (BookupWebView, bookup_web_view, WEBKIT_TYPE_WEB_VIEW)

enum
{
  PROP_CUSTOM_CSS = 1,
  PROP_CUSTOM_SCRIPT,
  N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

void
bookup_web_view_set_custom_css (BookupWebView *self, const char *css)
{
  WebKitUserContentManager *wk_manager;

  wk_manager = webkit_web_view_get_user_content_manager (WEBKIT_WEB_VIEW (self));

  if (self->custom_stylesheet)
    webkit_user_content_manager_remove_style_sheet (wk_manager, self->custom_stylesheet);

  self->custom_stylesheet = webkit_user_style_sheet_new (
      css,
      WEBKIT_USER_CONTENT_INJECT_ALL_FRAMES,
      WEBKIT_USER_STYLE_LEVEL_USER,
      NULL,
      NULL);

  webkit_user_content_manager_add_style_sheet (wk_manager, self->custom_stylesheet);
}

void
bookup_web_view_set_custom_script (BookupWebView *self, const char *script)
{
  WebKitUserContentManager *wk_manager;

  wk_manager = webkit_web_view_get_user_content_manager (WEBKIT_WEB_VIEW (self));

  if (self->custom_script)
    webkit_user_content_manager_remove_script (wk_manager, self->custom_script);

  self->custom_script = webkit_user_script_new (
      script,
      WEBKIT_USER_CONTENT_INJECT_ALL_FRAMES,
      WEBKIT_USER_SCRIPT_INJECT_AT_DOCUMENT_END,
      NULL,
      NULL);

  webkit_user_content_manager_add_script (wk_manager, self->custom_script);
}

static void
bookup_web_view_set_property (GObject *object,
                              guint prop_id,
                              const GValue *value,
                              GParamSpec *pspec)
{
  BookupWebView *self = BOOKUP_WEB_VIEW (object);

  switch (prop_id)
    {
    case PROP_CUSTOM_CSS:
      bookup_web_view_set_custom_css (self, g_value_get_string (value));
      break;
    case PROP_CUSTOM_SCRIPT:
      bookup_web_view_set_custom_script (self, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
constructed (GObject *obj)
{
  G_OBJECT_CLASS (bookup_web_view_parent_class)->constructed (obj);

  WebKitSettings *settings = webkit_web_view_get_settings (WEBKIT_WEB_VIEW (obj));
  webkit_settings_set_enable_developer_extras (settings, TRUE);
}

static gboolean
decide_policy_cb (G_GNUC_UNUSED WebKitWebView *web_view,
                  WebKitPolicyDecision *decision,
                  WebKitPolicyDecisionType type)
{
  g_autoptr (GError) error = NULL;
  WebKitURIRequest *request;
  const char *uri;

  switch (type)
    {
    case WEBKIT_POLICY_DECISION_TYPE_NAVIGATION_ACTION:
    case WEBKIT_POLICY_DECISION_TYPE_NEW_WINDOW_ACTION:
      {
        WebKitNavigationPolicyDecision *nav_decision;
        WebKitNavigationAction *nav_action;
        nav_decision = WEBKIT_NAVIGATION_POLICY_DECISION (decision);
        nav_action = webkit_navigation_policy_decision_get_navigation_action (nav_decision);
        request = webkit_navigation_action_get_request (nav_action);
        uri = webkit_uri_request_get_uri (request);

        g_debug ("Policy nav action for uri : %s", uri);

        if (g_str_has_prefix (uri, "file:///"))
          {
            webkit_policy_decision_use (decision);
          }
        else if (g_str_has_prefix (uri, "bookup:///"))
          {
            webkit_policy_decision_ignore (decision);
            // TODO: Load internal link
          }
        else
          {
            webkit_policy_decision_ignore (decision);

            if (!g_app_info_launch_default_for_uri (uri, NULL, &error))
              g_warning ("No default browser to launch urls: %s", error->message);
          }

        return TRUE;
      }
    case WEBKIT_POLICY_DECISION_TYPE_RESPONSE:
      {
        WebKitResponsePolicyDecision *response = WEBKIT_RESPONSE_POLICY_DECISION (decision);
        request = webkit_response_policy_decision_get_request (response);
        uri = webkit_uri_request_get_uri (request);

        g_debug ("Policy resp action for uri : %s", uri);

        webkit_policy_decision_ignore (decision);

        return TRUE;
      }
    }

  return FALSE;
}

static void
notify_user_content_manager_cb (BookupWebView *self)
{
  /* Load once the page.css */
  WebKitUserContentManager *wk_manager;
  g_autoptr (GFile) css_file = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (WebKitUserStyleSheet) style_sheet = NULL;
  g_autofree char *css = NULL;

  g_assert (BOOKUP_IS_WEB_VIEW (self));

  css_file = g_file_new_for_path (DATA_DIR "/bookup/page.css");

  if (!g_file_load_contents (css_file, NULL, &css, NULL, NULL, &error))
    {
      g_warning ("Error loading CSS file: %s", error->message);
      return;
    }

  wk_manager = webkit_web_view_get_user_content_manager (WEBKIT_WEB_VIEW (self));

  style_sheet = webkit_user_style_sheet_new (
      css,
      WEBKIT_USER_CONTENT_INJECT_ALL_FRAMES,
      WEBKIT_USER_STYLE_LEVEL_USER,
      NULL,
      NULL);

  webkit_user_content_manager_add_style_sheet (wk_manager, style_sheet);
}

static void
bookup_web_view_class_init (BookupWebViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->constructed = constructed;
  object_class->set_property = bookup_web_view_set_property;

  properties[PROP_CUSTOM_CSS] =
      g_param_spec_string ("custom-css",
                           "Custom css",
                           "User custom CSS",
                           NULL,
                           (G_PARAM_WRITABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));
  properties[PROP_CUSTOM_SCRIPT] =
      g_param_spec_string ("custom-script",
                           "Custom script",
                           "User custom javascript",
                           NULL,
                           (G_PARAM_WRITABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

static void
bookup_web_view_init (BookupWebView *self)
{
  g_signal_connect (self, "decide-policy", G_CALLBACK (decide_policy_cb), NULL);
  g_signal_connect_object (self,
                           "notify::user-content-manager",
                           G_CALLBACK (notify_user_content_manager_cb),
                           self, G_CONNECT_SWAPPED);
}

void
bookup_web_view_load_html (BookupWebView *self, const char *html)
{
  /*
   * We set the protocol file:/// as base_uri to be able to load local file
   * in the document
   */
  webkit_web_view_load_html (WEBKIT_WEB_VIEW (self), html, "file:///");
}

static gboolean
find_virtual_printer (GtkPrinter *printer, gpointer data)
{
  if (gtk_printer_is_virtual (printer))
    {
      *(GtkPrinter **) data = g_object_ref (printer);

      return TRUE;
    }

  return FALSE;
}

static char *
get_default_virtual_printer ()
{
  char *name = NULL;
  g_autoptr (GtkPrinter) printer = NULL;
  gtk_enumerate_printers (find_virtual_printer, &printer, NULL, TRUE);

  if (printer && GTK_IS_PRINTER (printer))
    {
      name = g_strdup (gtk_printer_get_name (printer));
      g_debug ("Virtual printer printer found : %s", name);
    }

  return name;
}

static void
print_failed (WebKitPrintOperation *op, GError *error)
{
  g_assert (WEBKIT_IS_PRINT_OPERATION (op));
  g_warning ("Print operation failed: %s", error->message);
}

gboolean
bookup_web_view_print (BookupWebView *self, const GFile *file)
{
  g_autofree char *file_uri = NULL;
  g_autoptr (GtkPrintSettings) settings = NULL;
  g_autoptr (WebKitPrintOperation) op = NULL;
  g_autofree char *virtual_printer = NULL;

  g_return_val_if_fail (BOOKUP_IS_WEB_VIEW (self), FALSE);

  file_uri = g_file_get_uri ((GFile *) file);
  op = webkit_print_operation_new (WEBKIT_WEB_VIEW (self));
  settings = gtk_print_settings_new ();

  virtual_printer = get_default_virtual_printer ();

  if (!virtual_printer)
    {
      GtkAlertDialog *alert = gtk_alert_dialog_new (_("Unable to find virtual printer"));
      GtkWidget *widget = gtk_widget_get_ancestor (GTK_WIDGET (self),
                                                   GTK_TYPE_APPLICATION_WINDOW);

      gtk_alert_dialog_show (alert, GTK_WINDOW (widget));

      return FALSE;
    }

  gtk_print_settings_set_printer (settings, virtual_printer);
  gtk_print_settings_set (settings, GTK_PRINT_SETTINGS_OUTPUT_FILE_FORMAT, "pdf");
  gtk_print_settings_set (settings, GTK_PRINT_SETTINGS_OUTPUT_URI, file_uri);

  g_signal_connect (op, "failed", G_CALLBACK (print_failed), NULL);

  webkit_print_operation_set_print_settings (op, settings);
  webkit_print_operation_print (op);

  return TRUE;
}
