## Summary

(Summarize the bug encountered concisely)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## System information

Installation type: Flatpak or manual

What is your operating system and version? _(e.g. "Linux, Fedora 36")_

What is the version of Bookup? _(e.g. "0.1.6.x86_64" or "commit 7e767814")_

## Relevant logs and/or screenshots

(Paste any relevant logs - use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

You can obtain debugging output information by launching Bookup with the environment var
'G_MESSAGES_DEBUG=all':

```
G_MESSAGES_DEBUG=all flatpak run org.gnome.gitlab.ilhooq.Bookup
```

/label ~"bug"
